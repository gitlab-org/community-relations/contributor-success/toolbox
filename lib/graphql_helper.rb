# frozen_string_literal: true

require 'httparty'
require 'json'

# A Mixin/Module for GraphQL
module GraphqlHelper
  def execute_graphql(query, variables: {})
    graphql_api_url = ENV.fetch('CI_API_GRAPHQL_URL', 'https://gitlab.com/api/graphql')
    gitlab_api_token = ENV.fetch('GITLAB_API_TOKEN', nil)

    authorization_header = gitlab_api_token.nil? ? {} : { 'Authorization' => "Bearer #{gitlab_api_token}" }

    cookie_hash = HTTParty::CookieHash.new
    cookie_hash.add_cookies('gitlab_canary=true')

    body = { query: query }
    body[:variables] = variables unless variables.empty?

    response = HTTParty.post(
      graphql_api_url,
      body: body.to_json,
      headers: {
        'Content-Type' => 'application/json',
        'Cookie' => cookie_hash.to_cookie_string,
        'User-Agent' => 'Contributor Success Toolbox'
      }.merge(authorization_header)
    )
    puts "Received a non-default error code #{response.code}" if response.code != 200
    JSON.parse(response.body)
  end

  # Auto-paginate a GraphQL query
  # @param [String] query the GraphQL query to execute
  # @param [Array<String>] path the path to the data to return
  # @param [Integer] max_pages the maximum number of pages to fetch. -1 for no limit
  # @param [Hash] variables the variables to pass to the query
  # @return [Array<Hash<String, Hash>>] the original GraphQL result, but with aggregated nodes from all pages
  # @note the `query` should have the `pageInfo` pagination and `after` fields
  def execute_graphql_autopaginated(query, path:, max_pages: -1, variables: {})
    raise "Query does not contain the necessary pagination fields. This method will not work as expected.\n\n#{query}" unless query.scan(/\$after|pageInfo|hasNextPage|endCursor/).size >= 5

    aggregated_nodes = []
    current_variables = variables.dup
    current_variables[:after] = nil

    puts
    print "Paginating over #{path.join('.')}"

    i = 0
    loop do
      i += 1
      break if max_pages.positive? && i > max_pages

      response = execute_graphql(query, variables: current_variables)
      raise "GraphQL Error: #{response['errors'].map { |e| e['message'] }.join(', ')}" if response['errors']

      print '.' # progress indicator

      data = response.dig(*path)
      aggregated_nodes.concat(data['nodes'])

      page_info = data['pageInfo']
      break unless page_info['hasNextPage']

      current_variables[:after] = page_info['endCursor']
    end

    print "\n" # end progress indicator
    aggregated_nodes
  end

  # Add a note to a notable
  # @param [String] notable_gid the GID of the notable
  # @param [String] body the body of the note
  # @return [Hash] the response from the GraphQL mutation
  def add_note(notable_gid, body)
    mutation = <<~EOGQL
      mutation addLabelToNotable {
        createNote(input: {
          noteableId: "#{notable_gid}"
          body: #{JSON.generate(body)}
        }) {
          errors
        }
      }
    EOGQL

    execute_graphql(mutation)
  end

  # Add a label to a notable
  # @param [String] notable_gid the GID of the notable
  # @param [String] label_name the name of the label
  # @return [Hash] the response from the GraphQL mutation
  def add_label_to_notable(notable_gid, label_name)
    add_note(notable_gid, %(/label ~"#{label_name}"))
  end
end
