# frozen_string_literal: true

require 'httparty'
require 'date'

# An API client for Discourse
class DiscourseApiClient
  BASE_URL = 'https://forum.gitlab.com'
  USERNAME = %w[leetickett-gitlab daniel-murphy stingrayza].sample
  DATE_STRING = Date.today.strftime('%Y-%m-%d')

  def initialize
    @discourse_api_key = ENV.fetch('DISCOURSE_API_KEY')
    @dry_run = ENV.fetch('DRY_RUN', '1')
  end

  def create_post(message)
    body = {
      title: "#{DATE_STRING} Weekly Community Contributor Thanks",
      raw: message,
      category: 39,
      tags: ['thanks']
    }

    puts body
    return unless @dry_run == '0'

    response = HTTParty.post(
      "#{BASE_URL}/posts.json",
      body: body.to_json,
      headers: {
        'Content-type' => 'application/json',
        'Api-Key' => @discourse_api_key,
        'Api-Username' => USERNAME
      }
    )
    puts response
  end
end
