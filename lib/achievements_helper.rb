# frozen_string_literal: true

require 'httparty'
require 'yaml'
require 'json'
require 'pry'

require_relative '../lib/graphql_helper'

# A helper for all things Achievements
# rubocop:disable Metrics/ModuleLength
module AchievementsHelper
  include GraphqlHelper

  def initialize(achievement_id, namespace_full_path: 'gitlab-org/achievements')
    @achievement_id = achievement_id
    @namespace_full_path = namespace_full_path
  end

  def dry_run?
    ENV.fetch('DRY_RUN', '1') != '0'
  end

  def execute
    raise NotImplementedError
  end

  def current_achievement_users
    return @current_achievement_users if @current_achievement_users

    users = []
    after = nil
    loop do
      puts 'grabbing a page of existing user achievements...'
      result = get_achievement_users_page(after)
      after = result[:end_cursor]
      users.concat(result[:users])

      break unless result[:has_next_page]
    end
    @current_achievement_users = users
  end

  def get_achievement_users_page(after)
    query = <<~EOGQL
      query getAchievements {
        namespace(fullPath: "#{namespace_full_path}") {
          achievements(ids: "#{achievement_gid}") {
            nodes {
              userAchievements(
                after: "#{after}"
                ) {
                nodes {
                  id
                  user {
                    username
                  }
                }
                pageInfo {
                  hasNextPage
                  endCursor
                }
              }
            }
          }
        }
      }
    EOGQL

    response = execute_graphql(query)
    user_achievements = response.dig('data', 'namespace', 'achievements', 'nodes')[0]['userAchievements']
    users = user_achievements['nodes'].map { |ua| { 'id' => ua['id'], 'username' => ua.dig('user', 'username').downcase } }
    puts "Got #{users.count} users with achievement #{achievement_id}"
    puts "\tfrom #{users.first['username']} to #{users.last['username']}" if users.any?

    has_next_page = user_achievements.dig('pageInfo', 'hasNextPage')
    end_cursor = user_achievements.dig('pageInfo', 'endCursor')

    { users: users, has_next_page: has_next_page, end_cursor: end_cursor }
  end

  def revoke_achievements(user_achievement_gids)
    user_achievement_gids.each do |gid|
      query = <<~EOGQL
        mutation revokeAchievement {
          achievementsRevoke(input: {
            userAchievementId: "#{gid}" }) {
              userAchievement {
                user {
                  username
                }
              }
            errors
          }
        }
      EOGQL

      log_and_execute("Revoking #{gid}") do
        puts execute_graphql(query)
      end
    end
  end

  # Count the number of achievements for a given username
  # @param [String] username
  # @return [Integer] the number of achievements awarded to the user
  def number_of_achievements_for(username)
    query = <<~EOGQL
      query numberOfAchievements($after: String) {
        user(username: "#{username}") {
          userAchievements(first: 100, after: $after) {
            nodes {
              achievement { id }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    EOGQL

    user_achievements = execute_graphql_autopaginated(query, path: %w[data user userAchievements])
    user_achievements&.count { |ua| ua&.dig('achievement', 'id') == achievement_gid } || 0
  end

  def log_and_execute(*msg)
    msg.prepend('DRY_RUN: ') if dry_run?
    puts msg.join

    yield unless dry_run?
  end

  def get_user_id(username)
    query = <<~EOGQL
      query getUser {
        user(username: "#{username}") {
          id
        }
      }
    EOGQL
    execute_graphql(query).dig('data', 'user', 'id')
  end

  # Award achievements to a list of usernames
  # @param [Enumerable<String>] usernames
  def award_achievements(usernames)
    usernames.each do |username|
      user_gid = get_user_id(username)
      query = <<~EOGQL
        mutation awardAchievement {
          achievementsAward(input: {
            achievementId: "#{achievement_gid}",
            userId: "#{user_gid}" }) {
              userAchievement {
                user {
                  username
                }
              }
              errors
          }
        }
      EOGQL

      log_and_execute("Award #{username}") do
        puts execute_graphql(query)
      end
    end
  end

  private

  attr_reader :achievement_id, :namespace_full_path

  def achievement_gid
    "gid://gitlab/Achievements::Achievement/#{achievement_id}"
  end
end
# rubocop:enable Metrics/ModuleLength
