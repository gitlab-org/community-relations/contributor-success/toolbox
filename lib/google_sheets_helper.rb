# frozen_string_literal: true

require 'google/apis/sheets_v4'
require 'googleauth'

# Interface for Google Sheets (wrapping the Google sheet API)
class GoogleSheetsHelper
  def initialize(sheet_id, sheet_name, google_service_account_credentials_path)
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.authorization = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: File.open(google_service_account_credentials_path),
      scope: 'https://www.googleapis.com/auth/drive'
    )
    @sheet_name = sheet_name
    @sheet_id = sheet_id
  end

  def read
    rows = @service.get_spreadsheet_values(@sheet_id, @sheet_name).values
    headers = rows.shift

    rows.map do |row|
      headers.zip(row).to_h
    end
  end

  def fetch_spreadsheet
    @service.get_spreadsheet(@sheet_id)
  end

  def add_new_tab(new_tab_name)
    batch_update_spreadsheet_request = Google::Apis::SheetsV4::BatchUpdateSpreadsheetRequest.new(
      requests: [
        {
          add_sheet: { properties: { title: new_tab_name } }
        }
      ]
    )
    @service.batch_update_spreadsheet(@sheet_id, batch_update_spreadsheet_request)
  end

  def clear_values
    clear_values_request = Google::Apis::SheetsV4::ClearValuesRequest.new
    range = "#{@sheet_name}!A1:Z"
    @service.clear_values(@sheet_id, range, clear_values_request)
  end

  def write_values(start_row, start_col, rows)
    row_num = start_row - 1

    all_values = rows.map do |row|
      row_num += 1
      end_col = (start_col.ord + row.length).chr

      Google::Apis::SheetsV4::ValueRange.new(
        range: "#{@sheet_name}!#{start_col}#{row_num}:#{end_col}#{row_num}",
        values: [row]
      )
    end

    batch_update_request = Google::Apis::SheetsV4::BatchUpdateValuesRequest.new(
      data: all_values,
      value_input_option: 'USER_ENTERED'
    )

    @service.batch_update_values(@sheet_id, batch_update_request).total_updated_cells
  end
end
