# frozen_string_literal: true

require 'gitlab'

# A helper class for wrapping the gitlab client
class GitlabClientHelper
  GITLAB_REST_API = 'https://gitlab.com/api/v4'

  def initialize(gitlab_token = '', base_url = GITLAB_REST_API)
    @gitlab_client = Gitlab.client(endpoint: base_url, private_token: gitlab_token)
  end

  def append_issue_description(issue, additional_description)
    current_description = issue.description
    new_description = "#{current_description}\n#{additional_description}"
    gitlab_client.edit_issue(issue.project_id, issue.iid, { description: new_description })
  end

  def get_issue(project_id, issue_id)
    gitlab_client.issue(project_id, issue_id)
  end

  def search_project_issues(project_id, search, options = {})
    options.merge!({ search: search })
    gitlab_client.issues(project_id, options).auto_paginate
  end

  def create_issue(project_id, title, options = {})
    gitlab_client.create_issue(project_id, title, options)
  end

  def group_merge_requests(group_id, options = {})
    gitlab_client.group_merge_requests(group_id, options).auto_paginate
  end

  def edit_issue(issue, params)
    gitlab_client.edit_issue(issue.project_id, issue.iid, params)
  end

  def get_group_members(group_id)
    gitlab_client.group_members(group_id, { per_page: 100 })
  end

  def create_issue_note(project_id, issue_id, message)
    gitlab_client.create_issue_note(project_id, issue_id, message)
  end

  def user_blocked_or_not_exists?(username)
    user = gitlab_client.user_search(nil, username: username)

    user.empty?
  end

  def name_from_username(username)
    gitlab_client.user_search(nil, username: username).first.name
  end

  attr_reader :gitlab_client
end
