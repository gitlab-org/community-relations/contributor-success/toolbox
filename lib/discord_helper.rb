# frozen_string_literal: true

require 'discordrb/webhooks'

# A Mixin/Module for Discord
module DiscordHelper
  BASE_URL = 'https://discord.com/api'

  def send_webhook(webhook_path, message_text)
    discord_client = Discordrb::Webhooks::Client.new(url: "#{BASE_URL}/#{webhook_path}")

    break_string(message_text).each do |message|
      discord_client.execute do |builder|
        builder.content = message
      end
    end
  rescue StandardError => e
    puts e.response
    raise
  end

  def break_string(str)
    max_length = 2_000

    return [str] if str.length <= max_length

    result = []

    until str.empty?
      if str.length <= max_length
        result << str.rstrip
        str = ''
      else
        break_index = str[0...max_length].rindex("\n")
        break_index ||= str[0...max_length].rindex(' ')
        raise StandardError, 'Discord message could not be broken down.' unless break_index

        result << str[0...break_index].rstrip
        str = str[break_index..].lstrip
      end
    end

    result
  end
end
