# frozen_string_literal: true

require 'httparty'
require 'yaml'

# A Mixin/Module for Team yml manipulation
module TeamYml
  WWW_GITLAB_COM_TEAM_YML = 'https://about.gitlab.com/company/team/team.yml'

  def team_members
    @team_members ||= team_from_www
  end

  def team_from_www
    team_users = {}
    fetch_yml(WWW_GITLAB_COM_TEAM_YML).each_with_object({}) do |item, _memo|
      next if exclude_member?(item)

      team_users[item['gitlab'].downcase] = {
        gitlab: item['gitlab'],
        name: item['name'],
        reports_to: item['reports_to'],
        role: item['role'],
        slug: item['slug'],
        username: item['gitlab'],
        projects: item['projects']
      }
    end
    team_users
  end

  def exclude_member?(item)
    item['role'] == 'Core Team member' || item['gitlab'].nil?
  end

  def fetch_yml(yaml_url)
    YAML.unsafe_load(HTTParty.get(yaml_url))
  end
end
