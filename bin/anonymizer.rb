#!/usr/bin/env ruby
# frozen_string_literal: true

require 'forgery'
require 'logger'

# Take a string of input, return anonymous values out
# No similarities to actual accounts intended
class DataAnonymizer
  INPUT_DATA = <<~EOSTRING
    :heart: :tada: Community Contribution Appreciation :tada: :heart:
    We are striving toward 1000 unique monthly wider community contributors and appreciate every effort from the community towards this goal.
    If you have any ideas/feedback/concerns please feel free to discuss them here!

    Thank you to all 50 wider community members who **AUTHORED** merge requests that were merged from 2023-10-09 to 2023-10-16.
    There were a total of 84 community contributions!

    Top authors (3+ merge requests :rocket:)
    <@56731246184438482642> (`@linktype`), <@54759531753449484132> (`@jaxspan`), Mark Fox (`@abatz`), Heather Porter (`@zoovu`), Joan Reed (`@podcat`), Willie Gilbert (`@latz`), Melissa Butler (`@divavu`)

    Regular authors (2 merge requests!)
    <@28939293365123757568> (`@podcat`), <@92865625742716794979> (`@yakidoo`), Anne Watkins (`@flashspan`), Marilyn Woods (`@oyonder`), Harold Berry (`@thoughtstorm`), Wayne Ruiz (`@voonder`), Larry Castillo (`@devpulse`), Ernest Reynolds (`@twinte`)

    Additional authors (1 merge request)
    <@23787858338321836195> (`@twinte`), <@26267412989413863872> (`@zoomcast`), <@67988365839472373154> (`@wordify`), <@46446528191439343919> (`@voonder`), <@23974998431275312991> (`@wordware`), Jennifer Lawson (`@vinder`), Susan Thompson (`@vipe`), Gloria Coleman (`@leexo`), Julie Clark (`@geba`), Steve Harper (`@photobean`), Russell Henry (`@avamm`), Sean Evans (`@centimia`), Michael Patterson (`@voolith`), Raymond Larson (`@jabbertype`), Ann Adams (`@zoovu`), Larry Bailey (`@plajo`), Virginia Snyder (`@feedspan`), Albert Mcdonald (`@wikido`), Catherine Robinson (`@janyx`), Douglas Hughes (`@tagcat`), George Mills (`@skaboo`), Amy Gonzales (`@voonder`), Johnny Harvey (`@voolith`), Mark Austin (`@yabox`), Jerry Lewis (`@lazz`), Amy Carr (`@tavu`), Irene Murphy (`@lazzy`), Phyllis Richards (`@skiptube`), Jose Larson (`@jumpxs`), Heather Ramirez (`@thoughtstorm`), Eugene Freeman (`@npath`), Ann James (`@mynte`), Judith Moore (`@miboo`), Steve Myers (`@aibox`), Shirley Gordon (`@photobean`), <@77326687369891212888> (`@bubbletube`), <@35444864314524894139> (`@zoonder`), <@84872893551411731189> (`@flipbug`), <@48781149565956644168> (`@meedoo`), <@79685759561939563193> (`@lazzy`), Jesse Jones (`@snaptags`), Roy Perry (`@wikizz`), Shawn Cruz (`@skinte`), Anthony Gomez (`@feedmix`), Pamela Adams (`@trudoo`), Katherine Williams (`@photobug`), Amanda Brooks (`@tambee`), Tammy Morrison (`@tambee`), Todd Henderson (`@livetube`), Jeffrey Rogers (`@shufflebeat`), Gerald Carr (`@dynava`), Philip Willis (`@jabbersphere`), Ralph Butler (`@livetube`), Gloria Franklin (`@bluezoom`), Henry Kim (`@devify`), Frank Williams (`@thoughtblab`), Raymond Sanders (`@tambee`), Katherine Woods (`@blognation`), Jack Gibson (`@mybuzz`), Alan Gardner (`@skiptube`), Kelly Matthews (`@skipstorm`), Gregory Washington (`@jabberbean`), Kathy Jenkins (`@aibox`), Julie Garza (`@blogtag`), Harold Woods (`@linktype`), Mildred Stone (`@youbridge`), Samuel Murphy (`@jabbersphere`), Todd Marshall (`@voolia`), Amanda Dean (`@kwimbee`), Catherine Lawrence (`@topicware`)

    ---
    Additionally, thank you to all 67 wider community members who participated/reviewed _other_ merge requests, merged from 2023-10-09 to 2023-10-16.

    Top performers (3+ interactions :rocket:)
    <@74366647886756376626> (`@vipe`), <@71516412828518291318> (`@shuffledrive`), <@39248722528575928937> (`@flipopia`), <@31988949886987423462> (`@skaboo`), Pamela Murphy (`@photobug`), Karen Wilson (`@realcube`), Frances Sanchez (`@abatz`), Judith Thomas (`@buzzdog`), Gloria Riley (`@devify`), Emily Allen (`@myworks`), Anne Dunn (`@chatterbridge`), Keith Gutierrez (`@topiczoom`), Harry Morgan (`@eazzy`), Matthew Cole (`@dynava`)

    High performers (2 interactions!)
    Kathy Gomez (`@ailane`), Jane Simmons (`@jamia`), Carl Morales (`@realblab`), Sarah Evans (`@zoomcast`), Eugene Bishop (`@brightbean`)

    Additional contributors (1 interaction)
    <@19673853793411783824> (`@dabfeed`), <@24311894651719633459> (`@voonte`), <@57258913969562168366> (`@devify`), <@89593517869746189659> (`@vinte`), <@98816881868448435926> (`@skiptube`), <@92867783758496534145> (`@latz`), <@65325413962967848542> (`@centizu`), Aaron Harrison (`@twitterbridge`), Kevin Mcdonald (`@digitube`), Andrew Lee (`@layo`), Karen Jordan (`@oyope`), Randy Reynolds (`@midel`), Diana Ruiz (`@plajo`), Amanda Gomez (`@yabox`), Patricia King (`@omba`), Jason Baker (`@trupe`), Marilyn Garcia (`@zooxo`), Sandra Hunt (`@fivechat`), Lawrence Reed (`@rooxo`), Peter Taylor (`@fadeo`), Joyce Welch (`@yambee`), Bonnie Nichols (`@oyoba`), Jane Walker (`@cogidoo`), Michelle Pierce (`@topdrive`), Kathleen Carpenter (`@oodoo`), Stephen Evans (`@gabvine`), Lois Simpson (`@dabshots`), Roger Morales (`@miboo`), Frances Chapman (`@miboo`), Marilyn White (`@avamba`), Rachel Romero (`@jabbercube`), Edward Sims (`@nlounge`), John Turner (`@eare`), Billy Thomas (`@dabz`), Susan Gutierrez (`@skyndu`), Phillip Ruiz (`@meetz`), Harold Oliver (`@buzzster`), Evelyn Hanson (`@janyx`), Patrick Wheeler (`@bluezoom`), Jason Ramos (`@feedfish`), Andrea Meyer (`@roombo`), Sandra Andrews (`@pixonyx`), Ruby Wright (`@jaxbean`), Janice Garcia (`@topicshots`), Craig Lee (`@thoughtstorm`), Douglas Hicks (`@livez`), Amanda Griffin (`@voonix`), Bobby Johnston (`@mynte`), <@43296873472642336337> (`@rhybox`), <@36987575222868236947> (`@zoomzone`), <@95793928991939862629> (`@skaboo`), <@19788233687989958228> (`@trudeo`), <@19621764614214931759> (`@trudoo`), <@67196924186478144539> (`@mycat`), <@68169552752289152121> (`@blognation`), Chris Clark (`@tagchat`), Ruby Mcdonald (`@browsedrive`), Sara Lawson (`@jetpulse`), Antonio Wright (`@youopia`), Donald Henderson (`@jazzy`), Pamela Stephens (`@babblestorm`), Joan Knight (`@photofeed`), Carl Myers (`@tazz`), Diana Richards (`@dabz`), Julia Hanson (`@skyndu`), Julia Gonzales (`@zoonder`), Eric Stevens (`@browsedrive`), Karen Clark (`@devbug`), Lawrence Weaver (`@rhyloo`), Jacqueline Harvey (`@jamia`), Marilyn Carroll (`@lajo`), Phyllis Watkins (`@aimbo`), Jesse Williamson (`@trudoo`), Andrew Cook (`@kazu`), Andrea Ramos (`@minyx`), Teresa Dean (`@avamm`), Wanda Morrison (`@rhynoodle`), Irene Moreno (`@browsedrive`), Russell Watkins (`@realmix`), Beverly Brooks (`@jamia`), Julie Chavez (`@pixonyx`), Edward Rose (`@shufflebeat`), Jack Rivera (`@flipbug`), Shirley Harvey (`@brainbox`), Thomas Harris (`@flipopia`), Rebecca Smith (`@twitterworks`), Cheryl Campbell (`@demivee`), Arthur Foster (`@skipstorm`), Laura Peters (`@photobean`), Maria Ramos (`@riffpath`), Julie Phillips (`@zooveo`), Carol Burns (`@skibox`), Antonio Wallace (`@myworks`), Phyllis Arnold (`@skiba`), Heather Hamilton (`@skipfire`), Raymond Morgan (`@abatz`)
  EOSTRING
  MATCH_REGEX = /(<@\d+>|[\p{L}\s()"]+) \(`@([\w.-]+)`\)(, )?/

  attr_reader :logger

  def initialize(logger = nil)
    @logger = logger || Logger.new($stdout)
    @logger.level = Logger::INFO
  end

  def fix_line(line)
    outline = ''

    line.scan(MATCH_REGEX).each do |ref|
      outline += ', ' if outline != ''
      # uses https://github.com/sevenwire/forgery to generate 'fake' names
      # Forgery(:russian_tax).account_number for a fake snowflake
      # Forgery(:name).full_name for a fake 'name'
      # Forgery(:name).company_name for a fake 'accountname'
      outline += if ref[0] =~ /<@\d+>/
                   "<@#{Forgery(:russian_tax).account_number}> (`@#{Forgery(:name).company_name.downcase}`)"
                 else
                   "#{Forgery(:name).full_name} (`@#{Forgery(:name).company_name.downcase}`)"
                 end
    end

    outline
  end

  def execute
    INPUT_DATA.split("\n").each do |line|
      line = fix_line(line) if MATCH_REGEX =~ line
      logger.info line
    end
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now

  anonymizer = DataAnonymizer.new
  anonymizer.execute

  anonymizer.logger.info "\nDone in #{Time.now - start} seconds."
end
