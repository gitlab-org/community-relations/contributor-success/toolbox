#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'

require_relative '../lib/graphql_helper'

# Grab all closed issues and their authors where they opted for tree rather than swag (labelled
# `reward::tree`) but not yet processed (not labelled `tree-achievement-awarded`).
# For each issue, award the achievement to the author and add the `tree-achievement-awarded` label.
class TreeAchievements
  include GraphqlHelper

  TREE_ACHIEVEMENT = 'gid://gitlab/Achievements::Achievement/1000069'

  def initialize
    @dry_run = ENV.fetch('DRY_RUN', '1')
  end

  def execute
    # Get the issues from the query
    issues = unprocessed_issues.dig('data', 'project', 'issues', 'nodes')
    puts "Found #{issues.count} issues with `reward::tree` and not `tree-achievement-awarded`"

    # Loop over the issues
    issues.each do |issue|
      author = issue['assignees']['nodes'].first
      puts "Awarding tree achievement to #{author['username']}"

      sleep(1)
      response = award_achievement(author['id'], TREE_ACHIEVEMENT)

      if response['errors']
        puts "Failed to award achievement: #{response['errors']}"
      else
        sleep(1)
        # Add label to issue
        add_label_to_issue(issue['id'])
      end
    end
  end

  def unprocessed_issues
    query = <<~EOGQL
      query issuesToAwardTreeAchievements
      {
        project(fullPath: "gitlab-org/developer-relations/contributor-success/rewards") {
          issues(
            labelName: ["reward::tree"]
            not: { labelName:"tree-achievement-awarded" }
            assigneeWildcardId: ANY
            state: closed
            first: 50
          ) {
            count
            nodes {
              id
              assignees {
                nodes {
                  username
                  id
                }
              }
            }
          }
        }
      }
    EOGQL

    response = execute_graphql(query)
    raise StandardError, "Failed to retreive issues: #{response['errors']}" if response['errors']

    response
  end

  def add_label_to_issue(issue_gid)
    query = <<~EOGQL
      mutation addLabelToIssue
      {
        createNote(input: {
          noteableId: "#{issue_gid}"
          body: "/label ~tree-achievement-awarded"
        }) {
          errors
        }
      }
    EOGQL

    # Skip action if dry run
    if @dry_run != '0'
      puts "Add label to issue #{issue_gid}"
      return {}
    end

    execute_graphql(query)
    # We don't check the response because it returns an error because the note wasn't persisted
    # (as it only consisted of quick actions).
  end

  def award_achievement(user_gid, achievement_gid)
    query = <<~EOGQL
      mutation awardAchievement {
        achievementsAward(input: {
          achievementId: "#{achievement_gid}",
          userId: "#{user_gid}" }) {
            errors
        }
      }
    EOGQL

    # Skip action if dry run
    if @dry_run != '0'
      puts "Award #{achievement_gid} to #{user_gid}"
      return {}
    end

    execute_graphql(query)
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  TreeAchievements.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
