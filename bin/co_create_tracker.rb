#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/google_sheets_helper'
require_relative '../lib/gitlab_client_helper'

# Track co-create ~type::feature merged and opened merge requests
class CoCreateTracker
  LEADING_ORGS_SHEET_NAME = 'MRARR Organization'
  COCREATE_ORGS_SHEET_NAME = 'Co-Create Pipeline DB'
  COCREATE_REPORT_HEADER = ['Company', 'CRM account id', 'Onsite', 'DRI', 'Merged co-create artifacts', 'Opened co-create artifacts', 'Notes'].freeze
  MAX_RETRIES = 3
  RETRY_DELAY = 5
  GITLAB_ORG_GROUP_ID = 9970

  def initialize
    # credentials
    @google_service_account_credentials_path = ENV.fetch('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS')
    @google_service_account_co_create_credentials_path = ENV.fetch('COCREATE_GOOGLE_SERVICE_ACCOUNT_CREDENTIALS')

    # sheet IDs
    @leading_orgs_tracker_sheet_id = ENV.fetch('LEADING_ORGS_TRACKER_SHEET_ID')
    @cocreate_orgs_tracker_sheet_id = ENV.fetch('CO_CREATE_ORGS_TRACKER_SHEET_ID')
    @cocreate_report_sheet_id = ENV.fetch('CO_CREATE_REPORT_SHEET_ID')

    @dry_run = ENV.fetch('DRY_RUN', '1') != '0'

    @period_start_date = ENV.fetch('PERIOD_START_DATE')
    @period_end_date = ENV.fetch('PERIOD_END_DATE')

    @period_start_date = DateTime.parse(@period_start_date) # DateTime.parse('2024-11-01T00:00:00.938Z')
    @period_end_date = DateTime.parse(@period_end_date) # DateTime.parse('2025-02-01T00:00:00.938Z')

    @report_tab_name = "#{@period_start_date.to_date} - #{@period_end_date.to_date}"
    @gitlab_client = GitlabClientHelper.new
  end

  def execute
    generate_report
  end

  private

  def generate_report
    data = co_create_report_data

    return puts 'Nothing to report!' if data.empty?

    clear_or_create_report_sheet
    populate_report_data(data)
  end

  def clear_or_create_report_sheet
    if report_sheet_exists?
      clear_report_sheet
    else
      create_report_sheet
    end
  end

  def fetch_merge_requests(contributor_usernames, mr_state, time_attribute)
    puts "- Fetching #{mr_state} merge requests..."

    contributor_usernames.reduce([]) do |merge_requests, username|
      retries = 0

      next merge_requests if username.empty?

      begin
        merge_requests << fetch_merge_requests_for(username, mr_state, time_attribute)
      rescue StandardError => e
        if retries < MAX_RETRIES
          retries += 1
          puts "An error occurred. Retrying in #{RETRY_DELAY} seconds. (Attempt #{retries}/#{MAX_RETRIES})"
          sleep RETRY_DELAY
          retry
        else
          puts "Failed to fetch #{mr_state} merge requests after #{MAX_RETRIES} attempts: #{e.message}"
          []
        end
      end
    end.flatten.compact
  end

  def fetch_merge_requests_for(username, state, time_attribute)
    @gitlab_client.group_merge_requests(
      GITLAB_ORG_GROUP_ID,
      {
        author_username: username,
        state: state,
        labels: 'type::feature'
      }
    ).map { |merge_request| merge_request['web_url'] if within_period?(merge_request[time_attribute]) }
  end

  def within_period?(date)
    return false unless date

    DateTime.parse(date).between?(@period_start_date, @period_end_date)
  end

  def co_create_report_data
    data = organization_data

    return [] if data.empty?

    data.reduce([COCREATE_REPORT_HEADER]) do |result, org|
      result << org.values
    end
  end

  def organization_data
    co_create_details = fetch_co_create_organizations

    fetch_mrarr_organizations.map do |organization|
      co_create_org_details = co_create_details.find { |org| org[:sfdc_id] == organization['SFDC_ACCOUNT_ID'].strip }

      next unless co_create_org_details

      puts "Processing #{organization['SFDC_ACCOUNT_ID']}"

      contributor_usernames = JSON.parse(organization['CONTRIBUTOR_USERNAMES'])

      org_merged_mrs = fetch_merge_requests(contributor_usernames, 'merged', 'merged_at')
      org_opened_mrs = fetch_merge_requests(contributor_usernames, 'opened', 'created_at')

      next if org_merged_mrs.empty? && org_opened_mrs.empty?

      co_create_org_details.merge({ merged_mrs: org_merged_mrs.join("\n"), opened_mrs: org_opened_mrs.join("\n") })
    end.compact
  end

  ##### Google sheets #####

  def fetch_co_create_organizations
    read_co_create_sheet.map do |organization|
      next if organization['Stage'] == 'Not Qualified'

      {
        organization: organization['Company'],
        sfdc_id: organization['CRM account id'],
        had_onsite: organization['Had onsite'],
        dri: organization['DRI (CSM or SA)']
      }
    end.compact
  end

  def read_co_create_sheet
    GoogleSheetsHelper.new(
      @cocreate_orgs_tracker_sheet_id,
      COCREATE_ORGS_SHEET_NAME,
      @google_service_account_co_create_credentials_path
    ).read
  end

  def fetch_mrarr_organizations
    GoogleSheetsHelper.new(
      @leading_orgs_tracker_sheet_id,
      LEADING_ORGS_SHEET_NAME,
      @google_service_account_credentials_path
    ).read
  end

  def report_sheet_exists?
    GoogleSheetsHelper.new(
      @cocreate_report_sheet_id,
      '',
      @google_service_account_co_create_credentials_path
    ).fetch_spreadsheet.sheets.find { |s| s.properties.title == @report_tab_name }
  end

  def clear_report_sheet
    puts "A report tab for the period #{@report_tab_name} already exist. Clearing..."

    return if @dry_run

    GoogleSheetsHelper.new(
      @cocreate_report_sheet_id,
      @report_tab_name,
      @google_service_account_co_create_credentials_path
    ).clear_values
  end

  def create_report_sheet
    puts "Creating a new report tab for the period #{@report_tab_name}"

    return if @dry_run

    GoogleSheetsHelper.new(
      @cocreate_report_sheet_id,
      '',
      @google_service_account_co_create_credentials_path
    ).add_new_tab(@report_tab_name)
  end

  def populate_report_data(data)
    puts 'Populating a new report data...'

    return if @dry_run

    GoogleSheetsHelper.new(
      @cocreate_report_sheet_id,
      @report_tab_name, # populating values to a new tab
      @google_service_account_co_create_credentials_path
    ).write_values(1, 'A', data)
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  CoCreateTracker.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
