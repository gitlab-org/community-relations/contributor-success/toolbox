#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/google_sheets_helper'
require_relative '../lib/gitlab_client_helper'

# Creates and updates issues for contributing organizations
class ContributingOrganizationIssues
  LEADING_ORGS_SHEET_NAME = 'MRARR Organization'
  TRACK_ORG_CONTRIBUTIONS_PROJECT_ID = 62_760_330

  def initialize
    token = ENV.fetch('GITLAB_API_TOKEN')
    raise ArgumentError, 'GITLAB_API_TOKEN is empty' if token.to_s.empty?

    @google_service_account_credentials_path = ENV.fetch('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS')
    @leading_orgs_tracker_sheet_id = ENV.fetch('LEADING_ORGS_TRACKER_SHEET_ID')
    @dry_run = ENV.fetch('DRY_RUN', '1')
    @gitlab_client = GitlabClientHelper.new(token)
  end

  def execute
    populate_issue_links(organization_issue_links)
  end

  private

  def organization_issue_links
    organizations.map do |row|
      process_organization_row(row)
    end
  end

  def process_organization_row(row)
    title = build_title(row)
    return process_existing_link(row, title) unless row['Issue link'].to_s.empty?

    process_new_issue(row, title)
  end

  def build_title(row)
    "#{row['CONTRIBUTOR_ORGANIZATION']} - #{row['SFDC_ACCOUNT_ID']}"
  end

  def process_new_issue(row, title)
    issue = find_issue(title, row['SFDC_ACCOUNT_ID'])

    if dry_run?
      generate_dry_run_message(issue, row)
    else
      issue ||= create_issue(title)
      issue.web_url
    end
  end

  def process_existing_link(row, title)
    issue = load_issue_from_link(row['Issue link'])
    return "Failed to load issue for account #{row['SFDC_ACCOUNT_ID']}" unless issue

    update_issue_if_needed(issue, title)
  end

  def generate_dry_run_message(issue, row)
    if issue
      "Found an existing issue for the account #{row['SFDC_ACCOUNT_ID']}"
    else
      "Creating a new issue for the account #{row['SFDC_ACCOUNT_ID']}"
    end
  end

  def dry_run?
    @dry_run == '1'
  end

  def parse_issue_from_link(issue_link)
    issue_link.match(%r{.*/-/issues/(\d+)})&.captures&.first
  end

  def load_issue_from_link(issue_link)
    issue_iid = parse_issue_from_link(issue_link)
    return nil unless issue_iid

    @gitlab_client.get_issue(TRACK_ORG_CONTRIBUTIONS_PROJECT_ID, issue_iid)
  rescue Gitlab::Error::NotFound
    puts "Issue not found: #{issue_link}"
    nil
  rescue StandardError => e
    puts "Error loading issue #{issue_link}: #{e.message}"
    nil
  end

  def update_issue_if_needed(issue, contributor_info)
    return issue.web_url if issue.title.include?(contributor_info)

    if dry_run?
      "Updating issue #{issue.web_url} with new contributor info: #{contributor_info}"
    else
      puts "Updating issue #{issue.web_url} with new contributor info: #{contributor_info}"
      @gitlab_client.edit_issue(issue, title: contributor_info).web_url
    end
  end

  def organizations
    GoogleSheetsHelper.new(
      @leading_orgs_tracker_sheet_id,
      LEADING_ORGS_SHEET_NAME,
      @google_service_account_credentials_path
    ).read
  end

  def find_issue(title, sfdc_id)
    puts "Searching for issue for account #{sfdc_id}..."
    @gitlab_client.search_project_issues(
      TRACK_ORG_CONTRIBUTIONS_PROJECT_ID,
      title,
      {
        in: 'title',
        state: 'opened'
      }
    )&.first
  end

  def create_issue(title)
    @gitlab_client.create_issue(TRACK_ORG_CONTRIBUTIONS_PROJECT_ID, title, { description: '/confidential' })
  end

  def populate_issue_links(issues_links)
    if @dry_run == '1'
      puts "Populating issue links #{issues_links}"
    else
      GoogleSheetsHelper.new(
        @leading_orgs_tracker_sheet_id,
        LEADING_ORGS_SHEET_NAME,
        @google_service_account_credentials_path
      ).write_values(2, 'C', issues_links.map { |link| [link] })
    end
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  ContributingOrganizationIssues.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
