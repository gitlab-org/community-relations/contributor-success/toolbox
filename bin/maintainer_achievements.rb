#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'yaml'
require 'json'
require 'pry'

require_relative '../lib/team_yml_helper'
require_relative '../lib/achievements_helper'

# Grab all team members
# Grab all user_achievements for the maintainer achievement

# Award the achievement to anyone who has not got it but is a maintainer
# Revoke the achievement from anyone who has it but is not a maintainer
class MaintainerAchievements
  include TeamYml
  include AchievementsHelper

  def execute
    puts 'DRY_RUN is enabled' if dry_run?

    should_have_achievement = active_maintainers
    have_achievement = current_achievement_users

    to_award_usernames = should_have_achievement - have_achievement.map { |u| u['username'] }
    puts "\nto_award_usernames:"
    puts to_award_usernames
    puts
    award_achievements(to_award_usernames)

    to_revoke = have_achievement.map { |u| u['username'] } - should_have_achievement
    puts "\nto_revoke:"
    puts to_revoke
    puts

    to_revoke_gids = to_revoke.map { |t| have_achievement.find { |h| h['username'] == t }['id'] }
    puts "\nto_revoke_gids:"
    puts to_revoke_gids
    puts
    revoke_achievements(to_revoke_gids)
  end

  private

  def active_maintainers
    team_from_www.select do |_, data|
      project_maintainer_or_owner?(data[:projects])
    end.keys
  end

  def project_maintainer_or_owner?(projects)
    roles = projects&.values&.compact
    return false unless roles

    (roles.flatten.map(&:split).flatten & %w[maintainer owner]).any?
  end
end

ACHIEVEMENT_ID = 2_000_176 # "Maintainer"

if $PROGRAM_NAME == __FILE__
  start = Time.now
  MaintainerAchievements.new(ACHIEVEMENT_ID).execute

  puts "\nDone in #{Time.now - start} seconds."
end
