#!/usr/bin/env ruby
# frozen_string_literal: true

require 'time'

require_relative '../../lib/team_yml_helper'
require_relative '../../lib/achievements_helper'

# Fast Review!
#
# This achievement is awarded to team members who have reviewed a merge request, in the GitLab.org / GitLab project, within 30 minutes of being assigned.
# @note this achievement can be awarded multiple times.
class FastReview
  include TeamYml
  include AchievementsHelper

  AUTOMATION_LABEL = 'automation:fast-review'

  # @note if this achievement is not awarded when it should have been, check to see if
  #   the team.yml file is up to date.
  def execute
    puts 'DRY_RUN is enabled' if dry_run?

    mrs_touched = []
    reviews_under_30_minutes(mrs_in_last_24_hours).each_with_object([]) do |(mr_gid, username), usernames_array|
      next unless team_members.include?(username)
      next if mrs_touched.include?(mr_gid)

      puts "Will award the achievement to #{username}"

      unless dry_run?
        award_achievements([username])
        add_note(mr_gid, <<~MARKDOWN)
          /label ~"#{AUTOMATION_LABEL}"
        MARKDOWN
      end

      usernames_array << username # Add the username to the result array
      mrs_touched << mr_gid
    end
  end

  private

  # Retrieve merge requests that have been updated in the past 24 hours in the GitLab project
  # that have at least one reviewer assigned to them
  # @return [Array<Hash>] merge objects from GraphQL
  # @note this does not consider reviews that are "Comments only". Only approvals and requested changes.
  # @note instead of pulling all notes immediately, we are only fetching those with reviews, so we are not polling all notes unnecessarily.
  def mrs_in_last_24_hours
    query = <<~GRAPHQL
      query getMRs($after: String) {
        project(fullPath: "gitlab-org/gitlab") {
          mergeRequests(
            updatedAfter: "#{(Time.now - (24 * 60 * 60)).iso8601}",
            after: $after,
            not: { labelName: "#{AUTOMATION_LABEL}" }
          ) {
            nodes {
              webUrl
              id
              author { username }
              reviewers { count }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    GRAPHQL

    mrs = execute_graphql_autopaginated(query, path: %w[data project mergeRequests])

    # reject MRs with no reviewers assigned
    mrs.reject! { |mr| mr.dig('reviewers', 'count').zero? }

    mrs_with_paginated_notes(mrs)
  end

  # Retrieve notes for each merge request and return an aggregated array of MRs with Nodes
  # @param mrs [Array<Hash>] merge requests
  # @return [Array<Hash>] merge requests with notes
  # @note This will only paginate a total of 1,000 notes per MR as a safety measure.
  def mrs_with_paginated_notes(mrs)
    mrs.each_with_object([]) do |mr, merge_requests|
      notes = execute_graphql_autopaginated(<<~GRAPHQL, max_pages: 10, path: %w[data mergeRequest notes], variables: { id: mr['id'] })
        query getNotes($id: MergeRequestID!, $after: String) {
          mergeRequest(id: $id) {
            notes(last: 100, after: $after, filter: ONLY_ACTIVITY) {
              nodes {
                body
                createdAt
                author { username }
              }
              pageInfo {
                hasNextPage
                endCursor
              }
            }
          }
        }
      GRAPHQL

      # only add the MR if the author has requested review
      merge_requests << mr.merge('notes' => notes) if notes.any? { |note| note['body'].match?(/requested review from @\w+/) }
    end
  end

  # Retrieve the usernames of reviewers who have completed a review within 30 minutes of being assigned
  # @param mrs [Array<Hash>] mrs with notes
  # @return [Array<Array<String>>] array of [gid, username] pairs
  # @note we filter out negative time differences.
  #       a negative time difference means the reviewer has reviewed before the request was made.
  #       whereas this is certainly desired, it's not a good way to go about awarding the achievements.
  def reviews_under_30_minutes(mrs)
    mrs.each_with_object([]) do |mr, reviews|
      mr_author = mr.dig('author', 'username')

      request_for_review_notes = mr['notes'].select { |note| note['body'].match?(/requested review from @\w+/) }
      review_notes = mr['notes'].select { |note| note['body'].match?(/approved this merge request|requested changes|left review comments/) }

      request_for_review_notes.each do |request_note|
        requested_at = request_note['createdAt']
        review_note_author = request_note.dig('author', 'username')

        match = request_note['body'].match(/requested review from ((?:@\w+\s*(?:and\s*)?)*)/)
        reviewers = match[1].split(/\s+and\s+/).map { |reviewer| reviewer.delete_prefix('@') }

        reviewers.each do |reviewer|
          # `find` will return the reviewer's first review and discard the rest
          review_note = review_notes.find do |note|
            note.dig('author', 'username') == reviewer
          end

          next unless review_note # the reviewer has not reviewed yet

          completed_at = review_note['createdAt']
          next if [mr_author, review_note_author].include?(reviewer) # self-reviews don't count
          next unless reviewed_in_time?(mr, reviewer, completed_at, requested_at)

          reviews << [mr['id'], reviewer]
        end
      end
    end
  end

  def reviewed_in_time?(merge_request, reviewer, completed_at, requested_at)
    time_diff = Time.parse(completed_at) - Time.parse(requested_at)

    return false if time_diff.negative? # reviewer reviewed before the request was made

    reviewed_in_time = time_diff <= 30 * 60

    puts "Review by #{reviewer} took #{(time_diff / 60).floor(2)} minutes for MR #{merge_request['webUrl']}" if reviewed_in_time
    reviewed_in_time
  end
end

ACHIEVEMENT_ID = 2_000_182 # "Fast review!"

if $PROGRAM_NAME == __FILE__
  start = Time.now
  FastReview.new(ACHIEVEMENT_ID).execute

  puts "\nDone in #{Time.now - start} seconds."
end
