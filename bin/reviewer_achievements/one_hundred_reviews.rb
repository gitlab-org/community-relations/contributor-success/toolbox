#!/usr/bin/env ruby
# frozen_string_literal: true

require 'time'

require_relative '../../lib/team_yml_helper'
require_relative '../../lib/achievements_helper'

# Achievement for 100 reviews.
# @note this achievement can be awarded multiple times.
# @note this achievement applies retroactively.
class OneHundredReviews
  include TeamYml
  include AchievementsHelper

  def execute
    puts 'DRY_RUN is enabled' if dry_run?

    mrs = mrs_in_last_24_hours
    reviewers = reviewers_with_100_review_milestones(mrs.flat_map { |mr| mr.dig('reviewers', 'nodes') })

    reviewers.each do |reviewer, review_count|
      next unless team_members.include?(reviewer)

      number_of_awards = number_of_achievements_for(reviewer)

      # calculate the difference
      number_to_award = (review_count / 100) - number_of_awards
      next if number_to_award.zero?

      if dry_run?
        puts "Would award #{reviewer} x#{number_to_award} for #{review_count} reviews (user currently has achievement x#{number_of_awards})"
        next
      end

      number_to_award.times do
        award_achievements([reviewer])
      end
    end
  end

  private

  # @return [Array<Hash>] merge requests updated in the last 24 hours that have been reviewed
  def mrs_in_last_24_hours
    query = <<~GRAPHQL
      query getReviews($after: String) {
        project(fullPath: "gitlab-org/gitlab") {
          mergeRequests(
            updatedAfter: "#{(Time.now - (24 * 60 * 60)).iso8601}",
            first: 25,
            after: $after
          ) {
            nodes {
              reviewers {
                nodes {
                  username
                  mergeRequestInteraction {
                    reviewState
                  }
                  reviewRequestedMergeRequests {
                    count
                  }
                }
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    GRAPHQL

    mrs = execute_graphql_autopaginated(query, path: %w[data project mergeRequests])

    # reject MRs with no reviews
    mrs.reject! { |mr| mr.dig('reviewers', 'nodes').empty? }

    mrs
  end

  # @return [Hash<String, Number>] usernames of reviewers with 100 review milestones
  def reviewers_with_100_review_milestones(reviewers)
    reviewers.each_with_object({}) do |reviewer, usernames|
      username = reviewer['username']
      count = reviewer.dig('reviewRequestedMergeRequests', 'count')

      next if %w[UNREVIEWED REVIEW_STARTED].include?(reviewer.dig('mergeRequestInteraction', 'reviewState')) # reviewer must have reviewed the MR
      next if count < 100 # reviewer must have at least 100 reviews

      usernames[username] = count
    end
  end
end

ACHIEVEMENT_ID = 2_000_183 # "100 reviews!"

if $PROGRAM_NAME == __FILE__
  start = Time.now
  OneHundredReviews.new(ACHIEVEMENT_ID).execute

  puts "\nDone in #{Time.now - start} seconds."
end
