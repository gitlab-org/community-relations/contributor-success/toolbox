#!/usr/bin/env ruby
# frozen_string_literal: true

require 'time'
require 'set'
require_relative '../../lib/team_yml_helper'
require_relative '../../lib/achievements_helper'

# This achievement is awarded to team members (of the gitlab-org group) that have merged an MR
# @note this is a one-time achievement
class Merger
  include TeamYml
  include AchievementsHelper

  def execute
    puts 'DRY_RUN is enabled' if dry_run?

    mergers = mergers_in_the_last_24_hours

    team_usernames = team_from_www.to_set(&:first)
    award_to = mergers.to_set { |m| m.dig('mergeUser', 'username') }
    award_to &= team_usernames
    award_to = remove_already_awarded(award_to)

    puts "Will award the achievement to the following users:\n- #{award_to.to_a.join("\n- ")}"

    if dry_run?
      puts 'DRY_RUN is enabled, skipping awarding achievements'
      return award_to
    end

    award_achievements(award_to)

    award_to
  end

  private

  # @return [Array<Hash>] an array of Mergers that have merged into the last 24 hours
  def mergers_in_the_last_24_hours
    query = <<~EOGQL
      query {
        group(fullPath: "gitlab-org") {
          mergeRequests(
            mergedAfter: "#{(Time.now - (24 * 60 * 60)).iso8601}",
            state: merged
          ) {
            nodes {
              mergeUser {
                username
              }
            }
          }
        }
      }
    EOGQL

    execute_graphql(query).dig('data', 'group', 'mergeRequests', 'nodes') || []
  end

  # Remove all users that already have the achievement
  # @param [Enumerable<String>] award_to the list of usernames to filter
  # @return [Enumerable<String>] the list of members to award to sans users that already have the achievement
  def remove_already_awarded(award_to)
    award_to.reject { |recipient| user_already_has_achievement?(recipient) }
  end

  # @param [String] username the username of the user to check
  # @return [Boolean] true if the user already has the achievement, false otherwise
  def user_already_has_achievement?(username)
    current_achievement_users.any? { |u| u['username'] == username }
  end
end

ACHIEVEMENT_ID = 2_000_181 # "Merger"

if $PROGRAM_NAME == __FILE__
  start = Time.now
  Merger.new(ACHIEVEMENT_ID).execute

  puts "\nDone in #{Time.now - start} seconds."
end
