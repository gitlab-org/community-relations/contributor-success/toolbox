#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'json'
require 'time'

require_relative '../lib/graphql_helper'

# Visits all community merge requests updated recently and adds the ~linked-issue label where appropriate.
# It would be nice if we could make this a reactive processor in triage-ops but we currently need to
# make an "expensive" API call.
# If the logic to extract issue references from the description was simpler, or the field(s) was present
# in the hook payload, we'd be onto a winner!
class MrLinkedIssueLabeller
  include GraphqlHelper

  attr_accessor :group_full_path, :updated_after, :error

  def initialize(group_full_path, updated_after)
    @group_full_path = group_full_path
    @updated_after = updated_after
    @error = false
    @production = ENV.fetch('PRODUCTION', '') == 'true'
    @rest_api_url = ENV.fetch('CI_API_V4_URL', 'https://gitlab.com/api/v4')
    @gitlab_api_token = ENV.fetch('GITLAB_API_TOKEN', '')
  end

  # We use a generic label to identify merge requests which are linked to issues
  LINKED_ISSUE_LABEL = 'linked-issue'

  def graphql_query(after)
    <<~EOGQL
      query {
        group(fullPath: "#{@group_full_path}") {
          mergeRequests(
            labels: ["Community contribution"]
            updatedAfter: "#{@updated_after.iso8601}"
            includeSubgroups: true
            after: "#{after}"
            ) {
            count
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              projectId
              iid
              webUrl
              labels {
                nodes {
                  title
                }
              }
            }
          }
        }
      }
    EOGQL
  end

  def update_linked_issue_label(project_id, merge_request_iid, already_labelled, has_linked_issue)
    return if already_labelled == has_linked_issue

    body = has_linked_issue ? "/label ~#{LINKED_ISSUE_LABEL}" : "/unlabel ~#{LINKED_ISSUE_LABEL}"
    puts " #{body}"

    return unless @production

    response = HTTParty.post(
      "#{@rest_api_url}/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes",
      body: { body: body }.to_json,
      headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{@gitlab_api_token}" }
    )
    puts " #{response.code}"
    @error = true unless response.success?
  end

  def linked_issue?(web_url)
    response = HTTParty.get(web_url)
    mr_widget_data_string = response.lines.select { |line| line.include?('window.gl.mrWidgetData =') }
    mr_widget_data = JSON.parse(mr_widget_data_string.first.slice(25..-1))
    mr_widget_data.dig('issues_links', 'closing_count').to_i.positive? || mr_widget_data.dig('issues_links', 'mentioned_count').to_i.positive?
  end

  def execute
    puts '==='
    puts group_full_path
    mrs = []
    after = nil
    loop do
      result = execute_graphql(graphql_query(after)).dig('data', 'group', 'mergeRequests')
      after = result.dig('pageInfo', 'endCursor')
      mrs.concat(result['nodes'])

      break unless result.dig('pageInfo', 'hasNextPage')
    end

    puts "Found mrs: #{mrs.count}"
    @error = true if mrs.count == 100

    mrs.each do |mr|
      web_url = mr['webUrl']
      puts web_url
      already_labelled = mr.dig('labels', 'nodes').any? { |label| label['title'] == LINKED_ISSUE_LABEL }
      puts " Already labelled: #{already_labelled}"
      has_linked_issue = linked_issue?(web_url)
      puts " Issue: #{has_linked_issue}"
      update_linked_issue_label(mr['projectId'], mr['iid'], already_labelled, has_linked_issue)
    end

    @error
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now

  # We want to allow time for the job to setup (pull docker image etc)
  # and also to allow for an occasional failure.
  buffer_minutes = 11 * 60
  updated_after = Time.now.utc - buffer_minutes
  puts "Grabbing mrs updated since #{updated_after}"

  error = false
  %w[
    gitlab-org
    gitlab-com
    gitlab-community
    components
  ].each do |group|
    error = true if MrLinkedIssueLabeller.new(group, updated_after).execute
  end

  puts '==='
  puts "Done in #{Time.now - start} seconds."

  raise 'Something went wrong!' if error
end
