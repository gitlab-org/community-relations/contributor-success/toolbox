#!/usr/bin/env ruby
# frozen_string_literal: true

require 'graphql/client'
require 'graphql/client/http'
require 'httparty'
require 'pry-byebug'

require_relative '../lib/gitlab_client_helper'
require_relative '../lib/team_yml_helper'

# Finds all merged merge requests a specific user participated in
class CommunityMrParticipation
  include TeamYml

  Client = GraphQL::Client

  GITLAB_API = 'https://gitlab.com/api/graphql'
  LABEL_COMMUNITY = 'Community contribution'
  USER_AGENT = "GitLab Quality Toolbox #{`git show --pretty=%H -q`.chomp}".freeze

  def initialize
    @start_date = Date.new(2023, 1, 1)
    @end_date = Date.new(2023, 11, 1)
    @token = ENV.fetch('GITLAB_API_TOKEN')
    @gitlab_client = GitlabClientHelper.new(@token)
    @participant_username = 'phikai'
  end

  def execute
    merge_requests = []
    count = 0

    # TODO: Consider adding `gitlab-community` at a later date (we don't currently use the `Community contribution` label, so it won't work at the moment)
    %w[gitlab-com gitlab-org].each do |group_path|
      query_vars = {
        from_date: @start_date,
        to_date: @end_date,
        end_cursor: '',
        page_size: 100,
        labels: [LABEL_COMMUNITY],
        group_path: group_path
      }
      loop do
        print '.'
        query_result = query(query_to_execute, resource_path: [], variables: query_vars)

        # is there a cleaner way?
        merge_requests += query_result.dig(:results, 'group', 'mergeRequests', 'edges')
        query_vars[:end_cursor] = query_result[:end_cursor] if query_result[:more_pages]
        count += 1

        break unless query_result[:more_pages]
      end
    end

    participated_mrs = process(merge_requests)
    puts participated_mrs.count
    pp participated_mrs
  end

  def query_to_execute
    <<~GRAPHQL
      query($from_date: Time, $to_date: Time, $end_cursor: String, $page_size: Int, $labels: [String!], $group_path: ID!) {
        group(fullPath: $group_path) {
          mergeRequests(
            state: merged,
            mergedAfter: $from_date,
            mergedBefore: $to_date,
            first: $page_size,
            after: $end_cursor,
            labels: $labels,
            includeSubgroups: true
          ) {
            edges {
              node {
                iid
                title
                mergedAt
                webUrl
                reviewers {
                  nodes {
                    username
                  }
                }
                approvedBy {
                  nodes {
                    username
                  }
                }
                commenters {
                  nodes {
                    username
                  }
                }
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    GRAPHQL
  end

  private

  def process(merge_requests)
    participated_mrs = []
    merge_requests.each do |mr_node|
      mr_details = mr_node['node']
      commented = username_in(mr_details['commenters'])
      reviewed = username_in(mr_details['reviewers'])
      approved = username_in(mr_details['approvedBy'])

      participated_mrs.push({ ur: mr_details['webUrl'], merged_at: mr_details['mergedAt'], commented: commented, reviewed: reviewed, approved: approved }) if commented || reviewed || approved
    end

    participated_mrs
  end

  def username_in(participants)
    participants['nodes'].each do |node|
      next if node.nil?

      return true if node['username'] == @participant_username
    end

    false
  end

  def query(base_query, resource_path: [], variables: {})
    graphql_query = client.parse(base_query)
    response = client.query(graphql_query, variables: variables, context: { token: @token })

    raise_on_error!(response, graphql_query, variables)

    parsed_response = parse_response(response, resource_path)
    headers = response.extensions.fetch('headers', {})

    raise 'One of the queries timed out. Please try again with a smaller page size.' if parsed_response.to_h.dig('group', 'mergeRequests', 'pageInfo').nil?

    pagination = parsed_response.to_h.dig('group', 'mergeRequests', 'pageInfo')

    graphql_response = {
      ratelimit_remaining: headers['ratelimit-remaining'].to_i,
      ratelimit_reset_at: Time.at(headers['ratelimit-reset'].to_i),
      more_pages: pagination['hasNextPage'],
      end_cursor: pagination['endCursor']
    }

    return graphql_response.merge(results: {}) if parsed_response.nil?

    graphql_response.merge(results: parsed_response.to_h)
  end

  def parse_response(response, resource_path)
    resource_path.reduce(response.data) { |data, resource| data&.send(resource) }
  end

  def raise_on_error!(response, graphql_query, variables)
    return if response.errors.empty?

    puts graphql_query
    puts variables
    puts "[DEBUG] #{response.inspect}"

    raise "There was an error: #{response.errors.messages.to_json}"
  end

  def http_client
    Client::HTTP.new(GITLAB_API) do
      def execute(document:, operation_name: nil, variables: {}, context: {}) # rubocop:disable Lint/NestedMethodDefinition
        body = {}
        body['query'] = document.to_query_string
        body['variables'] = variables if variables.any?
        body['operationName'] = operation_name if operation_name

        response = HTTParty.post(
          uri,
          body: body.to_json,
          headers: {
            'User-Agent' => USER_AGENT,
            'Content-type' => 'application/json',
            'PRIVATE-TOKEN' => context[:token]
          }
        )

        case response.code
        when 200, 400
          JSON.parse(response.body).merge('extensions' => { 'headers' => response.headers })
        else
          { 'errors' => [{ 'message' => "#{response.code} #{response.message}" }] }
        end
      end
    end
  end

  def schema
    @schema ||= Client.load_schema(http_client)
  end

  def client
    @client ||= Client.new(schema: schema, execute: http_client).tap { |client| client.allow_dynamic_queries = true }
  end
end

CommunityMrParticipation.new.execute if $PROGRAM_NAME == __FILE__
