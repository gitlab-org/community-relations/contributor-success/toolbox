#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/graphql_helper'

# Prints counts for defined labels for both open and closed issues in gitlab-org
class WorkflowCounts
  include GraphqlHelper

  LABELS = [
    'workflow::In review',
    'workflow::awaiting security release',
    'workflow::blocked',
    'workflow::canary',
    'workflow::complete',
    'workflow::design',
    'workflow::failed current version',
    'workflow::feature-flagged',
    'workflow::in dev',
    'workflow::in review',
    'workflow::in-review',
    'workflow::issue reviewed',
    'workflow::needs issue review',
    'workflow::open for discussion',
    'workflow::planning breakdown',
    'workflow::post-deploy-db-production',
    'workflow::post-deploy-db-staging',
    'workflow::problem validation',
    'workflow::production',
    'workflow::ready for design',
    'workflow::ready for development',
    'workflow::ready for review',
    'workflow::ready-for-review',
    'workflow::refinement',
    'workflow::scheduling',
    'workflow::solution validation',
    'workflow::staging',
    'workflow::staging-canary',
    'workflow::staging-ref',
    'workflow::start',
    'workflow::tw-test-status',
    'workflow::validation backlog',
    'workflow::verification',
    'workflow::version promoted'
  ].freeze

  def execute
    LABELS.each do |label|
      %w[opened closed].each do |status|
        query = <<~EOGQL
          query {#{' '}
            group(fullPath: "gitlab-org") {
              issues(
                includeSubgroups: true
                labelName: "#{label}"
                state: #{status}
              ) {
                count
              }
            }
          }
        EOGQL
        count = execute_graphql(query).dig('data', 'group', 'issues', 'count')
        puts "#{label} (#{status}): #{count}"
      end
    end
  end
end

WorkflowCounts.new.execute if $PROGRAM_NAME == __FILE__
