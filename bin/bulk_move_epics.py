#!env python3
import gitlab
import os
import time
from datetime import datetime

TOKEN = os.getenv('GITLAB_ACCESS_TOKEN')
gl = gitlab.Gitlab(private_token=TOKEN)

devrel_group_id = 3628337
de_group_id = 58184972
old_parent_iid = 98

q1_parent = 850224
q2_parent = 850225
q3_parent = 850227
q4_parent = 850228
q_parents = [q1_parent, q2_parent, q3_parent, q4_parent]

q1_date = datetime.strptime("2023-02-01", "%Y-%m-%d")
q2_date = datetime.strptime("2023-05-01", "%Y-%m-%d")
q3_date = datetime.strptime("2023-08-01", "%Y-%m-%d")
q4_date = datetime.strptime("2023-11-01", "%Y-%m-%d")
fy25_date = datetime.strptime("2024-02-01", "%Y-%m-%d")


dr_group = gl.groups.get(devrel_group_id)
de_group = gl.groups.get(de_group_id)

old_parent = dr_group.epics.get(old_parent_iid)

devrel_epics = dr_group.epics.list(iterator=True)
de_epics = de_group.epics.list(iterator=True)

def which_quarter(epic):
    if epic.start_date:
        epic_date = datetime.strptime(epic.start_date, "%Y-%m-%d")
        if q1_date <= epic_date < q2_date:
            print(f"  Moving epic to Q1 with a date of {epic.start_date}")
            epic.parent_id = q1_parent
        elif q2_date <= epic_date < q3_date:
            print(f"  Moving epic to Q2 with a date of {epic.start_date}")
            epic.parent_id = q2_parent
        elif q3_date <= epic_date < q4_date:
            print(f"  Moving epic to Q3 with a date of {epic.start_date}")
            epic.parent_id = q3_parent
        elif q4_date <= epic_date < fy25_date:
            print(f"  Moving epic to Q4 with a date of {epic.start_date}")
            epic.parent_id = q4_parent
        elif fy25_date <= epic_date:
            print(f"  Epic start date is in FY25 {epic.start_date}")
    else:
        print("  Epic start date is not set, skipping")

def epic_iterator(epic_list, group_id):
    count = 1
    for epic in epic_list:
        if epic.parent_id != old_parent.id or epic.id in q_parents:
            continue
        if epic.group_id != group_id:
            continue

        print(f"Found epic: {epic.iid} desc: {epic.title}")
        which_quarter(epic)
        epic.save()

        count += 1
        if count % 100 == 0:
            print("Sleeping for 5 seconds")
            time.sleep(5)
        if count > 50:
            break
    

epic_iterator(devrel_epics, devrel_group_id)
epic_iterator(de_epics, de_group_id)

