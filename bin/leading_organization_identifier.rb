#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/google_sheets_helper'
require_relative '../lib/graphql_helper'

# Grab all recently merged MRs
class LeadingOrganizationIdentifier
  include GraphqlHelper

  COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'
  LEADING_ORGS_TRACKER_SHEET_NAME = 'MRARR Org Concise'
  LEADING_ORGS_OVERRIDE_SHEET_NAME = 'Leading Organizations'
  LEADING_ORGS_MR_DETAIL_SHEET_NAME = 'MR Detail'
  LEADING_ORGS_USERS_SHEET_NAME = 'Leading Organization Users'
  QUALIFYING_MR_COUNT = 15
  PAGE_SIZE = 25

  def initialize
    today = Date.today
    end_of_last_month = today - today.day
    first_day_of_last_month = end_of_last_month - (end_of_last_month.day - 1)
    start_of_three_months_ago = first_day_of_last_month.prev_month(2)

    @start_date = ENV.fetch('START_DATE', start_of_three_months_ago)
    @end_date = ENV.fetch('END_DATE', end_of_last_month)
    @token = ENV.fetch('GITLAB_API_TOKEN')
    @google_service_account_credentials_path = ENV.fetch('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS')
    @leading_orgs_tracker_sheet_id = ENV.fetch('LEADING_ORGS_TRACKER_SHEET_ID')

    puts "Start date: #{@start_date}"
    puts "End date: #{@end_date}"
  end

  def graphql_query(group_path, end_cursor)
    <<~GRAPHQL
      query {
        group(fullPath: "#{group_path}") {
          mergeRequests(
            state: merged,
            mergedAfter: "#{@start_date}",
            mergedBefore: "#{@end_date}",
            first: #{PAGE_SIZE},
            after: "#{end_cursor}",
            labels: ["#{COMMUNITY_CONTRIBUTION_LABEL}"],
            includeSubgroups: true
          ) {
            nodes {
              author {
                username
              }
            }
            pageInfo {
              endCursor
              hasNextPage
            }
          }
        }
      }
    GRAPHQL
  end

  def execute
    detail = mr_detail
    write_mr_detail(detail)

    leading_orgs = leading_organization_overrides
    leading_orgs += detail.select { |row| row[:qualifies] && row[:org] }.map { |row| row[:org] }
    leading_org_users = users_from_orgs(leading_orgs, organization_usernames)
    leading_org_users += detail.select { |row| row[:qualifies] }.map { |row| row[:username] }
    write_leading_org_users(leading_org_users.uniq)
  end

  def write_leading_org_users(users)
    GoogleSheetsHelper.new(
      @leading_orgs_tracker_sheet_id,
      LEADING_ORGS_USERS_SHEET_NAME,
      @google_service_account_credentials_path
    ).write_values(2, 'A', users.map { |user| [user.downcase] }.sort)
  end

  def users_from_orgs(leading_orgs, organization_usernames)
    organization_usernames.select { |_key, value| leading_orgs.include?(value) }.map { |key, _value| key }
  end

  def write_mr_detail(detail)
    GoogleSheetsHelper.new(
      @leading_orgs_tracker_sheet_id,
      LEADING_ORGS_MR_DETAIL_SHEET_NAME,
      @google_service_account_credentials_path
    ).write_values(3, 'A', detail.sort_by { |row| row[:username].downcase }.map { |row| [row[:username].downcase, row[:org], row[:count], row[:qualifies]] })
  end

  def mr_detail
    authors = mr_authors
    overrides = leading_organization_overrides
    username_mappings = organization_usernames

    orgs = {}
    authors.each do |username, count|
      org = username_mappings[username]
      next unless org

      previous_count = orgs[org] || 0
      orgs[org] = previous_count + count
    end

    data = []
    authors.each do |username, count|
      org = username_mappings[username]
      qualifies = count >= QUALIFYING_MR_COUNT || overrides.include?(org) || (org && orgs[org] >= QUALIFYING_MR_COUNT) || false

      data << { username: username, org: org, count: count, qualifies: qualifies }
    end

    data
  end

  def leading_organization_overrides
    @leading_organization_overrides ||= GoogleSheetsHelper.new(
      @leading_orgs_tracker_sheet_id,
      LEADING_ORGS_OVERRIDE_SHEET_NAME,
      @google_service_account_credentials_path
    ).read.select { |row| row['DATE_OF_EXIT_OF_PROGRAM'] == '' }.map { |row| row['CONTRIBUTOR_ORGANIZATION'] }
  end

  def organization_usernames
    return @organization_usernames if @organization_usernames

    @organization_usernames = {}
    GoogleSheetsHelper.new(
      @leading_orgs_tracker_sheet_id,
      LEADING_ORGS_TRACKER_SHEET_NAME,
      @google_service_account_credentials_path
    ).read.each do |row|
      JSON.parse(row['CONTRIBUTOR_USERNAMES']).each do |username|
        @organization_usernames[username] = row['CONTRIBUTOR_ORGANIZATION']
      end
    end

    @organization_usernames
  end

  def mr_authors
    authors = {}
    %w[gitlab-com gitlab-org components gitlab-community].each do |group_path|
      end_cursor = ''
      print "\n"
      print "#{group_path}:"
      loop do
        print '.'
        query = graphql_query(group_path, end_cursor)
        query_result = execute_graphql(query)
        nodes = query_result.dig('data', 'group', 'mergeRequests', 'nodes')

        nodes.each do |merge_request|
          username = merge_request.dig('author', 'username')
          count = authors[username] || 0
          count += 1
          authors[username] = count
        end

        pagination = query_result.dig('data', 'group', 'mergeRequests', 'pageInfo')
        break unless pagination['hasNextPage']

        end_cursor = pagination['endCursor']
      end
    end

    authors
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  LeadingOrganizationIdentifier.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
