#!/usr/bin/env ruby
# frozen_string_literal: true

require 'gitlab/username_bot_identifier'
require 'yaml'

require_relative '../lib/graphql_helper'
require_relative '../lib/team_yml_helper'

# Generates a yml file containing the top contributors of a year
class YearlyTopContributors
  include GraphqlHelper
  include TeamYml

  TOP_CONTRIBUTOR_YAML = 'top_contributors.yml'

  def execute(year)
    groups = %w[gitlab-org gitlab-com gitlab-community components]
    merge_requests = groups.map { |group| get_merge_requests(group, year) }
    results = process_results(merge_requests.flatten)

    puts "Total merge requests: #{results[:mr_count]}"
    puts "Total users: #{results[:user_count]}"

    File.write(TOP_CONTRIBUTOR_YAML, results[:yaml].to_yaml)
    puts "Yaml written to #{TOP_CONTRIBUTOR_YAML}"
  end

  def process_results(merge_requests)
    merge_requests.reject! do |mr|
      username = mr.dig('author', 'username')
      Gitlab::UsernameBotIdentifier.new(username).ignorable_account? || team_members.keys.include?(username)
    end

    contributors_yaml = build_contributors_yaml(merge_requests)

    {
      mr_count: merge_requests.count,
      user_count: merge_requests.map { |mr| mr.dig('author', 'username') }.uniq.count,
      yaml: contributors_yaml
    }
  end

  def build_contributors_yaml(merge_requests)
    user_accumulator = {}

    merge_requests.map do |mr|
      user_id = mr.dig('author', 'id')
      user = user_accumulator[user_id] ||
             {
               'gitlab' => "https://gitlab.com/#{mr.dig('author', 'username')}",
               'name' => mr.dig('author', 'name'),
               'merged_mrs' => 0
             }
      user['merged_mrs'] += 1
      user_accumulator[user_id] = user
    end

    users = user_accumulator.values

    users.reject! { |user| user['merged_mrs'] < 5 }

    users.each do |user|
      user['category'] = if user['merged_mrs'] > 75
                           'SuperStar'
                         elsif user['merged_mrs'] > 10
                           'Star'
                         else
                           'Enthusiast'
                         end
    end

    users.sort_by { |user| -user['merged_mrs'] }
  end

  def get_merge_requests(group_path, year)
    merge_requests = []
    after = nil
    loop do
      result = get_merge_requests_page(group_path, year, after)
      after = result[:end_cursor]
      merge_requests.concat(result[:merge_requests])

      break unless result[:has_next_page]
    end

    merge_requests
  end

  def get_merge_requests_page(group_path, year, after)
    response = execute_graphql(get_graphql_query(group_path, year, after))

    merge_requests = response.dig('data', 'group', 'mergeRequests', 'nodes')

    has_next_page = response.dig('data', 'group', 'mergeRequests', 'pageInfo', 'hasNextPage')
    end_cursor = response.dig('data', 'group', 'mergeRequests', 'pageInfo', 'endCursor')

    return { merge_requests: [], has_next_page: has_next_page, end_cursor: end_cursor } if merge_requests.empty?

    puts "Got #{merge_requests.count} MRs merged between #{merge_requests.first['mergedAt']} to #{merge_requests.last['mergedAt']} in #{group_path}"
    { merge_requests: merge_requests, has_next_page: has_next_page, end_cursor: end_cursor }
  end

  def get_graphql_query(group_path, year, after)
    <<~GRAPHQL
      query {
        group(fullPath: "#{group_path}") {
          id
          mergeRequests(
            includeSubgroups: true
            mergedAfter: "#{year}-01-01"
            mergedBefore: "#{year}-12-31"
            labels: ["Community contribution"]
            after: "#{after}"
            state: merged
          ) {
            count
            nodes {
              author {
                id
                username
                name
              }
              mergedAt
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    GRAPHQL
  end
end

YearlyTopContributors.new.execute(ARGV.first.to_i) if $PROGRAM_NAME == __FILE__
