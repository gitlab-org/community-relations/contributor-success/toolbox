#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'pry-byebug'
require 'date'

require_relative '../lib/graphql_helper'

# This script extracts the pronouns of wider community contributors who have made a merge request in the last N days.
class CommunityPronouns
  include GraphqlHelper

  def initialize
    @days_ago = ENV.fetch('DAYS_AGO').to_i
  end

  def get_users(after, group_path, start_date)
    query = <<~EOGQL
      query {
        group(fullPath: "#{group_path}") {
          mergeRequests(
            labels: ["Community contribution"]
            createdAfter: "#{start_date}"
            after: "#{after}"
            includeSubgroups: true
            ) {
            nodes {
              author {
                id
                pronouns
              }
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
      }
    EOGQL

    records = execute_graphql(query).dig('data', 'group', 'mergeRequests')
    has_next_page = records.dig('pageInfo', 'hasNextPage')
    end_cursor = records.dig('pageInfo', 'endCursor')
    merge_requests = records['nodes']

    {
      has_next_page: has_next_page,
      end_cursor: end_cursor,
      merge_requests: merge_requests
    }
  end

  def execute
    users = []
    start_date = Date.today - @days_ago
    %w[gitlab-com gitlab-org].each do |group_path|
      after = nil
      loop do
        result = get_users(after, group_path, start_date)
        users.concat result[:merge_requests]

        print '.'
        break unless result[:has_next_page]

        after = result[:end_cursor]
      end
    end

    users.uniq { |user| user.dig('author', 'id') }.each do |user|
      id = user.dig('author', 'id')
      pronouns = user.dig('author', 'pronouns')
      puts "#{id}|#{pronouns}"
    end
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  CommunityPronouns.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
