#!/usr/bin/env ruby
# frozen_string_literal: true

require 'pry-byebug'

require_relative '../lib/team_yml_helper'

# Script to detect and report GitLab username errors in team.yml entries
class CheckTeamRedirects
  include TeamYml

  GITLAB_COM_URL_PREFIX = 'https://gitlab.com/'

  def execute
    puts "Processing #{team_members.count} records"
    team_members.each_key do |team_key|
      redirect = user_page_redirects(team_members[team_key][:gitlab])
      next if redirect == ''
      next if make_url(team_key) == redirect.downcase

      puts "#{team_members[team_key][:slug]} has a redirect from #{make_url(team_members[team_key][:gitlab])} to #{redirect}"
    end
  end

  def make_url(word)
    GITLAB_COM_URL_PREFIX + CGI.escape(word)
  end

  def strip_url(url)
    url.replace(GITLAB_COM_URL_PREFIX, '')
  end

  def user_page_redirects(user_name)
    user_page = make_url(user_name)
    response = HTTParty.head(user_page, follow_redirects: false)
    return response.headers['location'] if [301, 302].include? response.code

    ''
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now
  engine = CheckTeamRedirects.new
  engine.execute
  puts "\n Done in #{Time.now - start} seconds."
end
