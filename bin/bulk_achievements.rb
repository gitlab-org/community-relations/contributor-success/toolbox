#!/usr/bin/env ruby
# frozen_string_literal: true

require 'httparty'
require 'yaml'
require 'json'
require 'pry'

require_relative '../lib/graphql_helper'

# Take a list of usernames, get the user id for each and award an achievement
class BulkAchievements
  include GraphqlHelper

  NAMESPACE_FULL_PATH = 'gitlab-org'
  ACHIEVEMENT_ID = 53
  DRY_RUN = ENV.fetch('DRY_RUN', '1')

  def get_user_id(username)
    query = <<~EOGQL
      query getUser {
        user(username: "#{username}") {
          id
        }
      }
    EOGQL
    execute_graphql(query).dig('data', 'user', 'id')
  end

  def award_achievements(usernames)
    usernames.each do |username|
      user_gid = get_user_id(username)
      query = <<~EOGQL
        mutation awardAchievement {
          achievementsAward(input: {
            achievementId: "gid://gitlab/Achievements::Achievement/#{ACHIEVEMENT_ID}",
            userId: "#{user_gid}" }) {
              userAchievement {
                user {
                  username
                }
              }
              errors
          }
        }
      EOGQL

      puts "Award #{username}"
      puts execute_graphql(query) if DRY_RUN == '0'
    end
  end

  # rubocop:disable Metrics/MethodLength
  def execute
    usernames = %w[
      Taucher2003
      dlkeith90
      abitrolly
      X_Sheep
      zillemarco
      kyrie.31415926535
      feistel
      leetickett
      JeremyWuuuuu
      tuxtimo
      trakos
      fh1ch
      KevSlashNull
      wwwjon
      firelizzard
      feistel
      corneliusludmann
      espadav8
      sathieu
      leetickett
      yo
      dlouzan
      agravgaard
      yo
      gottesman.rachelg
      ksashikumar
      williams.brian-heb
      fh1ch
      gilangmlr
      jessehall3
      jacopo-beschi
      ksashikumar
      dm3ch
      steve.exley
      bufferoverflow
      fh1ch
      fh1ch
      gfyoung
      tuomoa
      ctabin
      fapapa
      g.grossetie
      q_wolphin
      kiameisomabes
      tnir
      Ravlen
      walkafwalka
      mortyccp
      wispfox
      tuomoa
      Qwertie
      gtsiolis
      jxterry
      jlemaes
      blackst0ne
      koffeinfrei
      rfwatson
      tnir
      hiroponz
      gtsiolis
      blackst0ne
      travismiller
      blackst0ne
      hiroponz
      koffeinfrei
      mgresko
      htve
      dosuken123
      innerwhisper
      jacopo-beschi
      Sanson
      toupeira
      Munken
      to1ne
      mahcsig
      munnerz
      ClemMakesApps
      winniehell
      ruianderson
      matt-oakes
      connorshea
      marcia
      artem-sidorenko
      koreamic
      gsmethells
      cristianbica
      zj
      bugagazavr
      stanhu
      stanhu
      cernvcs
      newton
      stanhu
      stanhu
      haynes
      cirosantilli
      bbodenmiller
      mvb
      mr-vinn
      razer6
      razer6
      jvanbaarsen
      skv-headless
      jvanbaarsen
      jhworth.developer
      dblessing
      jas.blanchard
      zzet
      StevenT
      smashwilson
      ialpert
      jadark
      karloxyz
      amacarthur
      hiroponz
      DanKnox
    ]

    award_achievements(usernames)
  end
end
# rubocop:enable Metrics/MethodLength

if $PROGRAM_NAME == __FILE__
  start = Time.now
  BulkAchievements.new.execute

  puts "\nDone in #{Time.now - start} seconds."
end
