# Contributor Success toolbox scripts

## Setup

```shell
› bundle install
› bundle exec lefthook install # for development only
```

### A note about tokens

Certain scripts may require a `read_api` access a token. This should be stored according to [the Token Management Standard](https://about.gitlab.com/handbook/security/token-management-standard.html#token-storage), ideally in [1Password](https://about.gitlab.com/handbook/security/password-guidelines.html#1password).

To access 1Password via CLI look here: https://developer.1password.com/docs/cli/get-started#install

You could dynamically pull this out of your 1Password vault with a command like the following. (This assumes a credential called "my gitlab pat" in the "Private" vault, with a "credential" value)

```shell
bundle exec bin/community_mr_participants.rb -s 2023-07-03 -e 2023-07-05 -w -t $(op read "op://Private/my gitlab pat/credential")
```

### WIP list of tokens used by the toolbox

| Token Name | Access Level | Scope | CI/CD Variable Name | Use |
| ---------- | ------------ | ----- | ------------------- | --- |
| READ_ONLY_GUEST | Guest | read_api | | |
| COMMUNITY_MR_BOT | Guest | api | COMMUNITY_MR_BOT_TOKEN | |
| TERRAFORM | Maintainer | api | GITLAB_TOKEN | Used by terraform to manage GitLab |
| Weekly team chores | Guest | api | TF_VAR_weekly_team_chores_token | Run weekly team chores job|

### Updating a token for a scheduled pipeline

1. Create new project access token [here](https://gitlab.com/gitlab-org/developer-relations/contributor-success/toolbox/-/settings/access_tokens?page=1).
1. Update the CI variable value. Documentation [here](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui).
1. Run master pipeline to sync the scheduled pipelines via Terraform.
1. Run scheduled pipeline and the new token should be in effect.

## History

This was forked from https://gitlab.com/gitlab-org/quality/toolbox on 2023-03-31 using:

```shell
git clone git@gitlab.com:gitlab-org/quality/toolbox.git
cd toolbox
git filter-repo --path .gitlab/ --path .gitignore --path .gitlab-ci.yml --path .rubocop.yml --path .rubocop_todo.yml --path CONTRIBUTING.md --path Gemfile --path Gemfile.lock --path LICENSE --path README.md --path bin/community_mr_participants.rb
git branch -m master main
```

As a result of using [`git filter-repo`](https://github.com/newren/git-filter-repo) we retained all change history on the files we kept, while removing all history of the other files from the original repo. 

## Scripts

### Community MR Participants

This script analyses all merged `Community contribution` MRs between the dates and
outputs a slack message recognising team members that have interacted with the wider
community. The slack message can be pasted into `#thanks` in slack.

This script is used as a part of the [Contributor Success Thanks message](https://www.gitlab.com/handbook/marketing/community-relations/contributor-success/community-contributors-workflows.html#contributor-thanks-messages) workflow.

```shell
> bundle exec bin/community_mr_participants.rb --help
Usage: bin/community_mr_participants.rb [options]

    -s, --startdate START_DATE       The start point for your search. yyyy-mm-dd (e.g. 2022-12-05)
    -e, --enddate END_DATE           The end point for your search. Only merge requests merged BEFORE this date will be included. Default: startdate + 7 days
    -w, --wider                      Run report for wider community
    -t, --token access_token         A valid access token
    -p, --disable-paging             Disable paging - will only execute a single query. Default: false
    -o, --sheet-output               Enable sheet (CSV) output. Default: false
    -c, --page-size count            Number of items to fetch per query. Default: 25
    -d, --debug                      Print debugging information
    -h, --help                       Print help message

Example usage:
- Weekly team member report (run on Monday, replacing yyyy-mm-dd with last Monday's date):
    bundle exec bin/community_mr_participants.rb -s yyyy-mm-dd -t $GITLAB_API_READ_TOKEN
- Monthly wider community report (run on the 22nd, replacing yyyy-mm-dd with the 22nd of last month, and YYYY-MM-DD with the 22nd of this month):
    bundle exec bin/community_mr_participants.rb -s yyyy-mm-dd -e YYYY-MM-DD -w -t $GITLAB_API_READ_TOKEN
```

### Community Pronouns

This script returns pronouns field data of users with MRs labeled as `Community contribution`
dating back however many days are entered to the `DAYS_AGO` variable.
Only public profiles return field data.
Private profiles are blank.
This script is used to monitor how many contributors are actually using the pronouns
field in their profiles against how many contributors do not use the pronouns field.

## Scheduled Pipelines

There are a (growing) number of scheduled pipelines configured for this project, that run some of the scripts on a regular interval. These are now managed in code under the `terraform/` directory.

If you wish to add a pipeline schedule, please add the schedule to this directory, and not manually on the project.
