# frozen_string_literal: true

require_relative '../bin/yearly_top_contributors'

RSpec.describe YearlyTopContributors do
  let(:instance) { described_class.new }
  let(:group_path) { 'group/path' }
  let(:year) { 2022 }

  describe '#execute' do
    let(:year) { 2021 }

    it 'writes data to a YAML file and outputs summary' do
      allow(instance).to receive(:get_merge_requests).and_return([])
      allow(instance).to receive(:puts)
      allow(File).to receive(:write)

      instance.execute(year)

      expect(File).to have_received(:write).with(described_class::TOP_CONTRIBUTOR_YAML, instance_of(String))
      expect(instance).to have_received(:puts).with(/Yaml written to/)
    end
  end

  describe '#process_results' do
    let(:merge_requests) do
      [
        { 'author' => { 'id' => 1, 'username' => 'bot', 'name' => 'Bot' }, 'mergedAt' => '2023-01-01' },
        { 'author' => { 'id' => 2, 'username' => 'team_member', 'name' => 'Team Member' }, 'mergedAt' => '2023-01-02' },
        { 'author' => { 'id' => 3, 'username' => 'community_member', 'name' => 'Community Member' }, 'mergedAt' => '2023-01-03' }
      ]
    end

    it 'filters out bot and team member merge requests' do
      ubi_false = instance_double(Gitlab::UsernameBotIdentifier, ignorable_account?: false)
      ubi_true = instance_double(Gitlab::UsernameBotIdentifier, ignorable_account?: true)
      allow(Gitlab::UsernameBotIdentifier).to receive(:new).and_return(ubi_false)
      allow(Gitlab::UsernameBotIdentifier).to receive(:new).with('bot').and_return(ubi_true)
      allow(instance).to receive(:team_members).and_return({ 'team_member' => true })

      result = instance.process_results(merge_requests)

      expect(result[:mr_count]).to eq(1)
    end
  end

  describe '#build_contributors_yaml' do
    let(:merge_requests) do
      4.times.map { { 'author' => { 'id' => 1, 'username' => 'a', 'name' => 'A' }, 'mergedAt' => '2023-01-03' } } +
        5.times.map { { 'author' => { 'id' => 2, 'username' => 'b', 'name' => 'B' }, 'mergedAt' => '2023-01-03' } } +
        10.times.map { { 'author' => { 'id' => 3, 'username' => 'c', 'name' => 'C' }, 'mergedAt' => '2023-01-03' } } +
        11.times.map { { 'author' => { 'id' => 4, 'username' => 'd', 'name' => 'D' }, 'mergedAt' => '2023-01-03' } } +
        75.times.map { { 'author' => { 'id' => 5, 'username' => 'e', 'name' => 'E' }, 'mergedAt' => '2023-01-03' } } +
        76.times.map { { 'author' => { 'id' => 6, 'username' => 'f', 'name' => 'F' }, 'mergedAt' => '2023-01-03' } }
    end

    it 'categorizes users based on their MR count' do
      result = instance.build_contributors_yaml(merge_requests)

      expect(result).to match [
        a_hash_including({ 'name' => 'F', 'category' => 'SuperStar' }),
        a_hash_including({ 'name' => 'E', 'category' => 'Star' }),
        a_hash_including({ 'name' => 'D', 'category' => 'Star' }),
        a_hash_including({ 'name' => 'C', 'category' => 'Enthusiast' }),
        a_hash_including({ 'name' => 'B', 'category' => 'Enthusiast' })
      ]
    end
  end

  describe '#get_merge_requests_page' do
    subject(:get_merge_requests_page) { instance.get_merge_requests_page(group_path, year, after) }

    let(:after) { nil }
    let(:has_next_page) { false }
    let(:end_cursor) { 'ec' }
    let(:merge_requests) { [] }

    let(:data) do
      {
        'data' => {
          'group' => {
            'mergeRequests' => {
              'nodes' => merge_requests,
              'pageInfo' => {
                'hasNextPage' => has_next_page,
                'endCursor' => end_cursor
              }
            }
          }
        }
      }
    end

    before do
      allow(instance).to receive(:get_graphql_query).and_return('query')
      allow(instance).to receive(:execute_graphql).and_return(data)
    end

    context 'with no merge requests' do
      it 'returns hash with empty merge_requests array' do
        expect(get_merge_requests_page).to eq({ merge_requests: [], has_next_page: has_next_page, end_cursor: end_cursor })

        expect(instance).to have_received(:get_graphql_query).with(group_path, year, after)
        expect(instance).to have_received(:execute_graphql).with('query')
      end
    end

    context 'with merge_requests' do
      let(:after) { 'ec' }
      let(:has_next_page) { true }
      let(:end_cursor) { 'pg' }

      let(:merge_requests) do
        [
          { 'author' => { 'id' => 1, 'username' => 'a', 'name' => 'A' }, 'mergedAt' => '2023-01-01' },
          { 'author' => { 'id' => 2, 'username' => 'b', 'name' => 'B' }, 'mergedAt' => '2023-02-01' }
        ]
      end

      it 'returns hash with expected merge_requests array' do
        allow(instance).to receive(:puts)

        expect(get_merge_requests_page).to eq({ merge_requests: merge_requests, has_next_page: has_next_page, end_cursor: end_cursor })

        expect(instance).to have_received(:puts).with('Got 2 MRs merged between 2023-01-01 to 2023-02-01 in group/path')
      end
    end
  end

  describe '#get_graphql_query' do
    let(:after) { 'ec' }

    it 'returns a string containing some expected strings' do
      result = instance.get_graphql_query(group_path, year, after)

      expect(result).to match(/fullPath: "#{group_path}"/)
      expect(result).to match(/mergedAfter: "#{year}-01-01"/)
      expect(result).to match(/mergedBefore: "#{year}-12-31"/)
      expect(result).to match(/after: "#{after}"/)
    end
  end

  describe '#get_merge_requests' do
    it 'handles paging and returns expected array' do
      allow(instance).to receive(:get_merge_requests_page).with(group_path, year, nil).and_return({ end_cursor: 'ec', merge_requests: [1, 2], has_next_page: true })
      allow(instance).to receive(:get_merge_requests_page).with(group_path, year, 'ec').and_return({ end_cursor: 'pg', merge_requests: [3, 4], has_next_page: false })

      expect(instance.get_merge_requests(group_path, year)).to contain_exactly(1, 2, 3, 4)
    end
  end
end
