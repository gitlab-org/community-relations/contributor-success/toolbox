# frozen_string_literal: true

require_relative '../../lib/google_sheets_helper'

RSpec.describe GoogleSheetsHelper do
  let(:credential_path) { '/credential_path' }
  let(:google_sheets_helper) do
    described_class.new(
      'SHEET_ID',
      'tab_name',
      credential_path
    )
  end

  before do
    file_content = <<-JSON
      {
        "type": "service_account",
        "project_id": "abc123",
        "private_key_id": "1111111111111111111111111111111111111111",
        "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAv4KNL8dZPL6h0tTP\\nL5WXti24v5osoVv4emCux6S2H4bu3ata8ppqHfcSsJSxiqlBq1sqqx3uVxkZDBuF\\nsJNE9QIDAQABAkBKRE+KUs15cBgDUcHTGzkNTifSLfDW1nrCwpGlHGwARz+3OwKl\\nSFPOVxAhzHdtaHjd/oGL50/qlp3PDJGNPAO1AiEA94o1SVjQ0NBLnJq4Lsm1yU0U\\nvHps+BN+O2jXfyXCXy8CIQDGDh90PZoBBrp5nL120ZeiPwkfObfIds9FP/bfOum1\\nGwIhAJhATJgJZZ4Zj3gJ/aDhdcsTet6WWjGXI7v8txbALbYHAiAaS3O3nhodOsR1\\nMu8goFEOdGoEoEgbMFLycbyYBJ1UswIgX7lVsqWb7WsCslr2kluFzP5aC9wMUvWG\\nUTSIXUN6mqk=\\n-----END PRIVATE KEY-----\\n",
        "client_email": "gitlab@example.com",
        "client_id": "111111111111111111111",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gitlab%40example.iam.gserviceaccount.com",
        "universe_domain": "googleapis.com"
      }
    JSON
    file = instance_double(File, read: file_content)
    allow(File).to receive(:open).with(credential_path).and_return(file)
  end

  describe '#fetch_spreadsheet' do
    it 'makes get_spreadsheet request' do
      expect(google_sheets_helper.instance_variable_get(:@service)).to receive(:get_spreadsheet).with('SHEET_ID')

      google_sheets_helper.fetch_spreadsheet
    end
  end

  describe '#add_new_tab' do
    it 'makes batch_update_spreadsheet request' do
      expect(google_sheets_helper.instance_variable_get(:@service)).to receive(:batch_update_spreadsheet)

      google_sheets_helper.add_new_tab('new tab')
    end
  end

  describe '#clear_values' do
    it 'makes clear_values request' do
      expect(google_sheets_helper.instance_variable_get(:@service)).to receive(:clear_values)

      google_sheets_helper.clear_values
    end
  end

  describe '#write_values' do
    it 'makes batch_update_values request' do
      expect(google_sheets_helper.instance_variable_get(:@service)).to receive(:batch_update_values).and_return(Google::Apis::SheetsV4::BatchUpdateValuesResponse.new)

      google_sheets_helper.write_values(1, 'A', [['a', 'b'], ['row 1', 'row2']])
    end
  end
end
