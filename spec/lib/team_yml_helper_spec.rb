# frozen_string_literal: true

require_relative '../../lib/team_yml_helper'

RSpec.describe TeamYml do
  include_context 'with team yml processing context'

  let(:concrete_class) do
    Class.new do
      include TeamYml

      def self.name
        'ConcreteTeamYmlClass'
      end
    end
  end

  let(:concrete_instance) { concrete_class.new }

  describe '#team_members' do
    it 'returns the processed team_yml' do
      expect(concrete_instance.team_members).to eq(parsed_team)
    end
  end
end
