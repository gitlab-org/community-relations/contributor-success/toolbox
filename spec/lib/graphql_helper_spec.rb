# frozen_string_literal: true

require_relative '../../lib/graphql_helper'

RSpec.describe GraphqlHelper do
  let(:concrete_class) do
    Class.new do
      include GraphqlHelper
    end
  end

  context 'when env variables are set' do
    before do
      stub_env('CI_API_GRAPHQL_URL', 'https://example.com/graphql')
      stub_env('GITLAB_API_TOKEN', 'token')
    end

    it 'makes HTTP POST request to correct url with correct headers and body (and parses returned JSON)' do
      stub_request(:post, 'https://example.com/graphql')
        .with(
          body: '{"query":"query {}"}',
          headers: {
            'Authorization' => 'Bearer token',
            'Content-Type' => 'application/json',
            'Cookie' => 'gitlab_canary=true',
            'User-Agent' => 'Contributor Success Toolbox'
          }
        )
        .to_return(status: 200, body: '{ "field": "value", "another": 1 }', headers: {})

      result = concrete_class.new.execute_graphql('query {}')

      expect(result).to include('field' => 'value', 'another' => 1)
    end

    context 'with a non-200 HTTP code' do
      it 'makes HTTP POST request to correct url with correct headers and body (and parses returned JSON)' do
        stub_request(:post, 'https://example.com/graphql')
          .with(
            body: '{"query":"query {}"}',
            headers: {
              'Authorization' => 'Bearer token',
              'Content-Type' => 'application/json',
              'Cookie' => 'gitlab_canary=true',
              'User-Agent' => 'Contributor Success Toolbox'
            }
          )
          .to_return(status: 500, body: '{"error": "random server error"}', headers: {})

        result = concrete_class.new.execute_graphql('query {}')

        expect(result).to include('error' => 'random server error')
      end
    end
  end

  it 'defaults to unauthenticated gitlab.com' do
    stub_request(:post, 'https://gitlab.com/api/graphql')
      .with(
        body: '{"query":"query {}"}',
        headers: {
          'Content-Type' => 'application/json',
          'Cookie' => 'gitlab_canary=true',
          'User-Agent' => 'Contributor Success Toolbox'
        }
      )
      .to_return(status: 200, body: '{}', headers: {})

    result = concrete_class.new.execute_graphql('query {}')

    expect(result).to eq({})
  end

  context 'when passing variables' do
    before do
      stub_env('CI_API_GRAPHQL_URL', 'https://example.com/graphql')
      stub_env('GITLAB_API_TOKEN', 'token')
    end

    it 'includes variables in the request' do
      stub_request(:post, 'https://example.com/graphql')
        .with(
          body: '{"query":"query {}","variables":{"var1":"value1"}}',
          headers: {
            'Content-Type' => 'application/json',
            'Cookie' => 'gitlab_canary=true',
            'User-Agent' => 'Contributor Success Toolbox'
          }
        )
        .to_return(status: 200, body: '{}', headers: {})

      result = concrete_class.new.execute_graphql('query {}', variables: { var1: 'value1' })

      expect(result).to eq({})
    end
  end

  describe '#execute_graphql_autopaginated' do
    let(:valid_query) { %(query test($after: String) { records(first: 3, after: $after) { pageInfo { hasNextPage endCursor } } }) }
    let(:invalid_query) { 'query {}' }

    before do
      stub_env('CI_API_GRAPHQL_URL', 'https://example.com/graphql')
      stub_env('GITLAB_API_TOKEN', 'token')

      stub_request(:post, 'https://example.com/graphql')
        .with(
          body: %({"query":"#{valid_query}","variables":{"after":null}}),
          headers: {
            'Content-Type' => 'application/json',
            'Cookie' => 'gitlab_canary=true',
            'User-Agent' => 'Contributor Success Toolbox'
          }
        )
        .to_return(status: 200, body: '{"data": { "records": { "nodes": [1, 2, 3], "pageInfo": { "hasNextPage": true, "endCursor": "cursor" } } } }', headers: {})

      stub_request(:post, 'https://example.com/graphql')
        .with(
          body: %({"query":"#{valid_query}","variables":{"after":"cursor"}}),
          headers: {
            'Content-Type' => 'application/json',
            'Cookie' => 'gitlab_canary=true',
            'User-Agent' => 'Contributor Success Toolbox'
          }
        )
        .to_return(status: 200, body: '{"data": { "records": { "nodes": [4, 5, 6], "pageInfo": { "hasNextPage": false, "endCursor": null } } } }', headers: {})
    end

    context 'when passing max_pages' do
      let(:clazz) { concrete_class.new }

      it 'queries only one page' do
        allow(clazz).to receive(:execute_graphql).and_call_original
        expect(clazz).to receive(:execute_graphql).once

        clazz.execute_graphql_autopaginated(valid_query, path: %w[data records], max_pages: 1)
      end
    end

    context 'when the query is missing pagination fields' do
      it 'raises an error' do
        expect { concrete_class.new.execute_graphql_autopaginated(invalid_query, path: %w[data records]) }.to raise_error(
          RuntimeError,
          /Query does not contain the necessary pagination fields. This method will not work as expected./
        )
      end
    end

    it 'aggregates nodes from all pages' do
      result = concrete_class.new.execute_graphql_autopaginated(valid_query, path: %w[data records])

      expect(result).to eq([1, 2, 3, 4, 5, 6])
    end
  end
end
