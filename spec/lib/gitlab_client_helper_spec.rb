# frozen_string_literal: true

require 'spec_helper'

require_relative '../../lib/gitlab_client_helper'

RSpec.describe GitlabClientHelper do
  subject(:client) do
    described_class.new('test-token', 'https://gitlab.example.com/api/v4')
  end

  let(:gitlab_client) do
    instance_double(
      Gitlab::Client,
      create_issue_note: note,
      edit_issue: issue,
      issues: Gitlab::PaginatedResponse.new([issue]),
      create_issue: issue2,
      group_members: found_user,
      issue: issue
    )
  end
  let(:issue) do
    Gitlab::ObjectifiedHash.new(
      {
        description: 'Current description',
        title: 'Issue 1',
        project_id: project_id,
        iid: issue_iid,
        web_url: 'https://example/web_url'
      }
    )
  end
  let(:issue2) do
    Gitlab::ObjectifiedHash.new(
      {
        description: 'Current description',
        title: 'Issue 2',
        project_id: project_id,
        iid: issue_iid + 1,
        web_url: 'https://example/web_url'
      }
    )
  end
  let(:note) do
    Gitlab::ObjectifiedHash.new(
      {
        id: note_id,
        body: 'note body',
        noteable_id: note_id,
        project_id: project_id
      }
    )
  end
  let(:found_user) do
    [
      Gitlab::ObjectifiedHash.new(
        {
          id: 123,
          username: 'user-found',
          name: 'Real user',
          state: 'activeuser',
          web_url: 'https://example/user-found'
        }
      )
    ]
  end
  let(:project_id) { 123 }
  let(:group_id) { 456 }
  let(:issue_iid) { 678 }
  let(:note_id) { 910 }
  let(:description_addition) { "\nextra stuff" }

  before do
    allow(Gitlab).to receive(:client).and_return(gitlab_client)
  end

  describe '#append_issue_description' do
    it 'calls gitlab_client.edit_issue with the expected params' do
      client.append_issue_description(issue, description_addition)

      expect(client.gitlab_client).to have_received(:edit_issue).with(project_id, issue_iid, { description: "#{issue.description}\n#{description_addition}" })
    end
  end

  describe '#get_issue' do
    it 'fetches an issue' do
      result = client.get_issue(project_id, issue_iid)

      expect(client.gitlab_client).to have_received(:issue).with(project_id, issue_iid)
      expect(result).to eq(issue)
    end
  end

  describe '#search_project_issues' do
    it 'calls gitlab_client.issues with with expected arguments' do
      client.search_project_issues(project_id, 'Issue 1', { state: 'opened', in: 'title' })

      expect(client.gitlab_client).to have_received(:issues).with(project_id, { state: 'opened', search: 'Issue 1', in: 'title' })
    end
  end

  describe '#create_issue' do
    it 'calls gitlab_client.create_issue_note with with expected arguments' do
      title = 'Issue 2'
      client.create_issue(project_id, title)

      expect(client.gitlab_client).to have_received(:create_issue).with(project_id, title, {})
    end
  end

  describe '#edit_issue' do
    it 'edits an issue' do
      client.edit_issue(issue, { due_date: '2023-09-04' })

      expect(client.gitlab_client).to have_received(:edit_issue).with(project_id, issue_iid, { due_date: '2023-09-04' })
    end
  end

  describe '#group_merge_requests' do
    it 'calls gitlab_client.group_merge_requests with with expected arguments' do
      allow(client.gitlab_client).to receive(:group_merge_requests).with(group_id, {}).and_return(Gitlab::PaginatedResponse.new([]))

      expect(client.group_merge_requests(group_id)).to be_empty
    end
  end

  describe '#get_group_members' do
    it 'fetches group members' do
      result = client.get_group_members(group_id)

      expect(client.gitlab_client).to have_received(:group_members).with(group_id, per_page: 100)
      expect(result).to eq(found_user)
    end
  end

  describe '#create_issue_note' do
    it 'creates an issue note' do
      message = 'Test note'
      result = client.create_issue_note(project_id, issue_iid, message)

      expect(client.gitlab_client).to have_received(:create_issue_note).with(project_id, issue_iid, message)
      expect(result).to eq(note)
    end
  end

  describe '#user_blocked_or_not_exists?' do
    it 'returns true if user not found' do
      allow(gitlab_client).to receive(:user_search).with(nil, { username: 'user-not-found' }).and_return([])

      user = client.user_blocked_or_not_exists?('user-not-found')

      expect(user).to be_truthy
    end

    it 'returns false if user found' do
      allow(gitlab_client).to receive(:user_search).with(nil, { username: 'user-found' }).and_return(:found_user)

      user = client.user_blocked_or_not_exists?('user-found')

      expect(user).to be_falsey
    end
  end

  describe '#name_from_username' do
    it 'returns the users name' do
      allow(gitlab_client).to receive(:user_search).with(nil, { username: 'user-found' }).and_return(found_user)

      user = client.name_from_username('user-found')

      expect(user).to eq('Real user')
    end
  end
end
