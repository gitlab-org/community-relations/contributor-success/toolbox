# frozen_string_literal: true

require_relative '../../bin/reviewer_achievements/merger'

RSpec.describe Merger do
  let(:merger) { described_class.new(1) }
  let(:team_yml) { YAML.safe_load(load_fixture('team_members.yml')) }
  let(:graphql_response) do
    {
      'data' => {
        'group' => {
          'mergeRequests' => {
            'nodes' => [
              { 'mergeUser' => { 'bot' => true, 'username' => 'user1' } },
              { 'mergeUser' => { 'bot' => false, 'username' => 'user2' } },
              { 'mergeUser' => { 'bot' => false, 'username' => 'user3' } },
              { 'mergeUser' => { 'bot' => false, 'username' => 'user4' } },
              { 'mergeUser' => { 'bot' => false, 'username' => 'user5' } },
              { 'mergeUser' => { 'bot' => false, 'username' => 'user6' } } # non-team member
            ]
          }
        }
      }
    }
  end

  before do
    allow(merger).to receive_messages(execute_graphql: graphql_response, fetch_yml: team_yml)
  end

  describe '#execute' do
    subject(:execution) { merger.execute }

    context 'when the user already has the achievement' do
      before do
        allow(merger).to receive(:current_achievement_users).and_return([{ 'username' => 'user2' }])
      end

      it { is_expected.to contain_exactly('user3', 'user4', 'user5') }
    end

    context 'when the user is a bot' do
      let(:graphql_response) do
        {
          'data' => {
            'group' => {
              'mergeRequests' => {
                'nodes' => [
                  { 'mergeUser' => { 'bot' => true, 'username' => 'user1' } }
                ]
              }
            }
          }
        }
      end

      it { is_expected.to be_empty }
    end

    context 'when the user is a team member and does not have the achievement' do
      before do
        allow(merger).to receive(:current_achievement_users).and_return([])
      end

      it { is_expected.to contain_exactly('user2', 'user3', 'user4', 'user5') }

      it 'awards achievements' do
        stub_env('DRY_RUN', '0')

        expect(merger).to receive(:award_achievements).with(%w[user2 user3 user4 user5])
        execution
      end
    end

    context 'when the user is a team member and has the achievement' do
      before do
        allow(merger).to receive(:current_achievement_users).and_return([{ 'username' => 'user2' }, { 'username' => 'user3' }])
      end

      it { is_expected.to contain_exactly('user4', 'user5') }
    end
  end
end
