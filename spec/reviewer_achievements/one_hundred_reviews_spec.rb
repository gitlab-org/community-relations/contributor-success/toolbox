# frozen_string_literal: true

require_relative '../../bin/reviewer_achievements/one_hundred_reviews'

RSpec.describe OneHundredReviews do
  let(:one_hundred_reviews) { described_class.new(1) }
  let(:team_yml) { YAML.safe_load(load_fixture('team_members.yml')) }
  let(:standardized_response) do
    [
      {
        'reviewers' => {
          'nodes' => [
            { 'username' => 'user1', 'mergeRequestInteraction' => { 'reviewState' => 'UNREVIEWED' }, 'reviewRequestedMergeRequests' => { 'count' => 100 } }, # bot
            { 'username' => 'user2', 'mergeRequestInteraction' => { 'reviewState' => 'REVIEWED' }, 'reviewRequestedMergeRequests' => { 'count' => 1_234 } },
            { 'username' => 'user3', 'mergeRequestInteraction' => { 'reviewState' => 'REQUESTED_CHANGES' }, 'reviewRequestedMergeRequests' => { 'count' => 100 } },
            { 'username' => 'user4', 'mergeRequestInteraction' => { 'reviewState' => 'REVIEW_STARTED' }, 'reviewRequestedMergeRequests' => { 'count' => 40 } },
            { 'username' => 'user5', 'mergeRequestInteraction' => { 'reviewState' => 'APPROVED' }, 'reviewRequestedMergeRequests' => { 'count' => 99 } },
            { 'username' => 'user6', 'mergeRequestInteraction' => { 'reviewState' => 'UNAPPROVED' }, 'reviewRequestedMergeRequests' => { 'count' => 0 } } # non-team member
          ]
        }
      },
      {
        'reviewers' => {
          'nodes' => [
            { 'username' => 'user3', 'mergeRequestInteraction' => { 'reviewState' => 'APPROVED' }, 'reviewRequestedMergeRequests' => { 'count' => 101 } }
          ]
        }
      }
    ]
  end

  before do
    allow(one_hundred_reviews).to receive_messages(execute_graphql_autopaginated: standardized_response, fetch_yml: team_yml)
  end

  describe '#execute' do
    subject(:execution) { one_hundred_reviews.execute }

    context 'when the user is due achievements' do
      before do
        allow(one_hundred_reviews).to receive(:current_achievement_users).and_return([{ 'username' => 'user2' }])
        allow(one_hundred_reviews).to receive(:number_of_achievements_for).with('user2').and_return(10) # already awarded 10 times, needs 2 more
        allow(one_hundred_reviews).to receive(:number_of_achievements_for).with('user3').and_return(0)
      end

      it 'awards the achievement' do
        stub_env('DRY_RUN', '0')

        expect(one_hundred_reviews).to receive(:award_achievements).with(['user2']).twice
        expect(one_hundred_reviews).to receive(:award_achievements).with(['user3']).once

        expect(execution).to contain_exactly(['user2', 1_234], ['user3', 101])
      end
    end
  end
end
