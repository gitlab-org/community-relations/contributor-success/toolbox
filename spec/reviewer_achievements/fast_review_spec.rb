# frozen_string_literal: true

require_relative '../../bin/reviewer_achievements/fast_review'

RSpec.describe FastReview do
  let(:fast_review) { described_class.new(1) }
  let(:team_yml) { YAML.safe_load(load_fixture('team_members.yml')) }

  let(:mrs) do
    [
      { 'webUrl' => '1', # MR that was reviewed within 30 minutes of the review being requested
        'id' => 'gid://gitlab/MergeRequest/1',
        'reviewers' => { 'count' => 1 },
        'author' => { 'username' => 'user1' } },
      { 'webUrl' => '2', # MR that was not reviewed within 30 minutes
        'id' => 'gid://gitlab/MergeRequest/2',
        'reviewers' => { 'count' => 1 },
        'author' => { 'username' => 'user2' } },
      { 'webUrl' => '3', # MR that was reviewed within 30 minutes by the author of the MR
        'id' => 'gid://gitlab/MergeRequest/3',
        'reviewers' => { 'count' => 1 },
        'author' => { 'username' => 'user3' } },
      { 'webUrl' => '4', # MR that was reviewed within 30 minutes by someone not assigned as a reviewer
        'id' => 'gid://gitlab/MergeRequest/4',
        'reviewers' => { 'count' => 2 },
        'author' => { 'username' => 'user4' } },
      { 'webUrl' => '5', # MR that is authored by one user, and another user assigns themselves to review.
        'id' => 'gid://gitlab/MergeRequest/5',
        'reviewers' => { 'count' => 2 },
        'author' => { 'username' => 'user3' } },
      { 'webUrl' => '6', # MR that assigns multiple reviewers
        'id' => 'gid://gitlab/MergeRequest/6',
        'reviewers' => { 'count' => 2 },
        'author' => { 'username' => 'user3' } }
    ]
  end

  let(:mr_notes) do
    [
      [
        { 'body' => 'requested review from @user2',
          'createdAt' => '2025-01-01T00:00:00Z',
          'author' => { 'username' => 'user1' } },
        { 'body' => 'approved this merge request',
          'createdAt' => '2025-01-01T00:15:00Z', # reviewed 15 minutes later
          'author' => { 'username' => 'user2' } }
      ],
      [
        { 'body' => 'requested review from @user3',
          'createdAt' => '2025-01-01T00:00:00Z',
          'author' => { 'username' => 'user2' } },
        { 'body' => 'requested changes',
          'createdAt' => '2025-01-01T00:31:00Z', # reviewed 31 minutes later
          'author' => { 'username' => 'user3' } }
      ],
      [
        { 'body' => 'requested review from @user3',
          'createdAt' => '2025-01-01T00:00:00Z',
          'author' => { 'username' => 'user3' } },
        { 'body' => 'requested changes',
          'createdAt' => '2025-01-01T00:15:00Z', # the author reviewed their own MR 15 minutes later
          'author' => { 'username' => 'user3' } }
      ],
      [
        { 'body' => 'requested review from @user5',
          'createdAt' => '2025-01-01T00:00:00Z',
          'author' => { 'username' => 'user5' } },
        { 'body' => 'approved this merge request',
          'createdAt' => '2025-01-01T00:15:00Z', # reviewed 15 minutes later
          'author' => { 'username' => 'user3' } } # user3 approved but is not assigned to review
      ],
      [
        { 'body' => 'approved this merge request',
          'createdAt' => '2025-01-01T00:15:00Z',
          'author' => { 'username' => 'user4' } },
        { 'body' => 'requested review from @user4',
          'createdAt' => '2025-01-01T00:16:00Z',
          'author' => { 'username' => 'user4' } } # self-assigned review of another's MR
      ],
      [
        { 'body' => 'requested review from @user2 and @user5',
          'createdAt' => '2025-01-01T00:00:00Z',
          'author' => { 'username' => 'user3' } },
        { 'body' => 'approved this merge request',
          'createdAt' => '2025-01-01T00:15:00Z',
          'author' => { 'username' => 'user2' } }, # user2 reviewed this MR also within 30 minutes
        { 'body' => 'approved this merge request',
          'createdAt' => '2025-01-01T00:31:00Z',
          'author' => { 'username' => 'user5' } }
      ]
    ]
  end

  before do
    allow(fast_review).to receive(:fetch_yml).and_return(team_yml)
    allow(fast_review).to receive(:execute_graphql_autopaginated).with(anything, path: %w[data project mergeRequests]).and_return(mrs)

    mr_notes.size.times do |i|
      allow(fast_review).to receive(:execute_graphql_autopaginated)
        .with(anything, path: %w[data mergeRequest notes], max_pages: anything, variables: { id: "gid://gitlab/MergeRequest/#{i + 1}" })
        .and_return(mr_notes[i])
    end
  end

  describe '#execute' do
    subject(:execution) { fast_review.execute }

    it 'awards the achievements and adds the notes', :aggregate_failures do
      stub_env('DRY_RUN', '0')

      # user2 reviewed MRs 1 and 5 in time
      expect(fast_review).to receive(:award_achievements).with(%w[user2]).twice
      expect(fast_review).to receive(:add_note).once.with('gid://gitlab/MergeRequest/1', include(described_class::AUTOMATION_LABEL))
      expect(fast_review).to receive(:add_note).once.with('gid://gitlab/MergeRequest/6', include(described_class::AUTOMATION_LABEL))

      expect(execution).to contain_exactly('user2', 'user2')
    end

    context 'when DRY_RUN' do
      it 'awards the achievement', :aggregate_failures do
        # user2 reviewed MRs 1 and 5 in time
        expect(fast_review).not_to receive(:award_achievements)
        expect(fast_review).not_to receive(:add_note)

        expect(execution).to contain_exactly('user2', 'user2')
      end
    end
  end
end
