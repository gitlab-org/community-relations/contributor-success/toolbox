# frozen_string_literal: true

require_relative '../bin/leading_organization_identifier'

RSpec.describe LeadingOrganizationIdentifier do
  let(:credential_path) { '/credential/path' }

  before do
    stub_env('CI_API_GRAPHQL_URL', 'https://gitlab.com/api/graphql')
    stub_env('GITLAB_API_TOKEN', 'gitlab-access-token')
    stub_env('START_DATE', '2024-02-01')
    stub_env('END_DATE', '2024-02-29')
    stub_env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS', credential_path)
    stub_env('LEADING_ORGS_TRACKER_SHEET_ID', 'SHEET_ID')

    # NOTE: This private key is generated and not real!
    file_content = <<-JSON
      {
        "type": "service_account",
        "project_id": "abc123",
        "private_key_id": "1111111111111111111111111111111111111111",
        "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAv4KNL8dZPL6h0tTP\\nL5WXti24v5osoVv4emCux6S2H4bu3ata8ppqHfcSsJSxiqlBq1sqqx3uVxkZDBuF\\nsJNE9QIDAQABAkBKRE+KUs15cBgDUcHTGzkNTifSLfDW1nrCwpGlHGwARz+3OwKl\\nSFPOVxAhzHdtaHjd/oGL50/qlp3PDJGNPAO1AiEA94o1SVjQ0NBLnJq4Lsm1yU0U\\nvHps+BN+O2jXfyXCXy8CIQDGDh90PZoBBrp5nL120ZeiPwkfObfIds9FP/bfOum1\\nGwIhAJhATJgJZZ4Zj3gJ/aDhdcsTet6WWjGXI7v8txbALbYHAiAaS3O3nhodOsR1\\nMu8goFEOdGoEoEgbMFLycbyYBJ1UswIgX7lVsqWb7WsCslr2kluFzP5aC9wMUvWG\\nUTSIXUN6mqk=\\n-----END PRIVATE KEY-----\\n",
        "client_email": "gitlab@example.com",
        "client_id": "111111111111111111111",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gitlab%40example.iam.gserviceaccount.com",
        "universe_domain": "googleapis.com"
      }
    JSON
    file = instance_double(File, read: file_content)
    allow(File).to receive(:open).with(credential_path).and_return(file)

    stub_request(:post, 'https://www.googleapis.com/oauth2/v4/token').to_return(
      {
        status: 200,
        headers: { 'Content-Type': 'application/json' },
        body: '{"access_token":"token","expires_in":3599,"token_type":"Bearer"}'
      }
    )
  end

  describe '#mr_authors', vcr: { match_requests_on: %i[method uri headers body] } do
    it 'returns expected users and counts' do
      authors = described_class.new.mr_authors

      expect(authors.length).to eq(148)
      expect(authors['PatrickRice']).to eq(8)
      expect(authors['gerardo-navarro']).to eq(9)
    end
  end

  describe '#organization_usernames', :vcr do
    it 'returns expected users and organizations' do
      usernames = described_class.new.organization_usernames

      expect(usernames.length).to be(6)
      expect(usernames['the']).to eq('The Lazy')
      expect(usernames['over']).to eq('ABC Co.')
    end
  end

  describe '#leading_organization_overrides', :vcr do
    it 'returns expected organizations' do
      overrides = described_class.new.leading_organization_overrides

      expect(overrides.length).to be(2)
      expect(overrides).to include('Leading')
      expect(overrides).to include('Leader')
    end
  end

  describe '#mr_detail' do
    let(:instance) { described_class.new }

    before do
      allow(instance).to receive(:mr_authors).and_return(
        {
          'the' => 2,
          'quick' => 5,
          'brown' => 5,
          'fox' => 4,
          'jumps' => 1,
          'dogs' => 21,
          'lazy' => 6,
          'moon' => 14
        }
      )
      allow(instance).to receive(:organization_usernames).and_return(
        {
          'the' => 'Leading',
          'over' => 'Leading',
          'quick' => 'ABC Co.',
          'brown' => 'ABC Co.',
          'fox' => 'ABC Co.',
          'jumps' => 'ABC Co.',
          'lazy' => 'ZZZ Org'
        }
      )
      allow(instance).to receive(:leading_organization_overrides).and_return(%w[Leading Leader])
    end

    it 'returns expected author data' do
      data = instance.mr_detail

      expect(data.length).to eq(8)
      expect(data).to include(a_hash_including(username: 'the', org: 'Leading', count: 2, qualifies: true))
      expect(data).to include(a_hash_including(username: 'quick', org: 'ABC Co.', count: 5, qualifies: true))
      expect(data).to include(a_hash_including(username: 'brown', org: 'ABC Co.', count: 5, qualifies: true))
      expect(data).to include(a_hash_including(username: 'fox', org: 'ABC Co.', count: 4, qualifies: true))
      expect(data).to include(a_hash_including(username: 'jumps', org: 'ABC Co.', count: 1, qualifies: true))
      expect(data).to include(a_hash_including(username: 'dogs', count: 21, qualifies: true))
      expect(data).to include(a_hash_including(username: 'lazy', org: 'ZZZ Org', count: 6, qualifies: false))
      expect(data).to include(a_hash_including(username: 'moon', count: 14, qualifies: false))
    end
  end

  describe '#write_mr_detail', :vcr do
    it 'makes the expected http calls' do
      described_class.new.write_mr_detail(
        [
          { count: 2, org: 'Leading', qualifies: true, username: 'the' },
          { count: 5, org: 'ABC Co.', qualifies: true, username: 'Quick' },
          { count: 5, org: 'ABC Co.', qualifies: true, username: 'brown' },
          { count: 5, org: 'ABC Co.', qualifies: true, username: 'fox' },
          { count: 5, org: 'ABC Co.', qualifies: true, username: 'jumps' },
          { count: 21, org: nil, qualifies: true, username: 'dogs' },
          { count: 6, org: 'ZZZ Org', qualifies: false, username: 'lazy' },
          { count: 7, org: nil, qualifies: false, username: 'moon' }
        ]
      )
    end
  end

  describe '#users_from_orgs' do
    it 'returns the expected result' do
      leading_organization_overrides = ['ABC Co.', 'Leading Org']
      organization_usernames = {
        'the' => 'Some Co.',
        'quick' => 'ABC Co.',
        'brown' => 'Leading Org',
        'fox' => 'ZZZ Ltd',
        'jumps' => 'Some Co.',
        'over' => 'ABC Co.',
        'lazy' => 'Leading Org'
      }

      users_from_orgs = described_class.new.users_from_orgs(leading_organization_overrides, organization_usernames)

      expect(users_from_orgs).to contain_exactly('quick', 'brown', 'over', 'lazy')
    end
  end

  describe '#write_leading_org_users', :vcr do
    it 'makes the expected http calls' do
      users = %w[the QuIck brown]

      described_class.new.write_leading_org_users(users)
    end
  end

  describe '#execute' do
    let(:mr_detail) do
      [
        { count: 2, org: 'Leading', qualifies: true, username: 'the' },
        { count: 5, org: 'ABC Co.', qualifies: true, username: 'quick' },
        { count: 5, org: 'ABC Co.', qualifies: true, username: 'brown' },
        { count: 5, org: 'ABC Co.', qualifies: true, username: 'fox' },
        { count: 5, org: 'ABC Co.', qualifies: true, username: 'jumps' },
        { count: 21, org: nil, qualifies: true, username: 'dogs' },
        { count: 6, org: 'ZZZ Org', qualifies: false, username: 'lazy' },
        { count: 7, org: nil, qualifies: false, username: 'moon' }
      ]
    end

    let(:leading_org_users) do
      %w[the quick brown fox jumps dogs over rainbow]
    end

    let(:instance) { described_class.new }

    before do
      allow(instance).to receive(:mr_detail).and_return(mr_detail)
      allow(instance).to receive(:write_mr_detail)

      allow(instance).to receive(:leading_organization_overrides).and_return(['Leading'])
      allow(instance).to receive(:organization_usernames).and_return(
        {
          'the' => 'Leading',
          'quick' => 'ABC Co.',
          'brown' => 'ABC Co.',
          'fox' => 'ABC Co.',
          'rainbow' => 'ABC Co.',
          'over' => 'Leading'
        }
      )

      allow(instance).to receive(:write_leading_org_users)
    end

    it 'calls the expected methods with the expected values' do
      instance.execute

      expect(instance).to have_received(:write_mr_detail).with(mr_detail)
      expect(instance).to have_received(:write_leading_org_users).with(match_array(leading_org_users))
    end
  end
end
