# frozen_string_literal: true

require_relative '../bin/tree_achievements'

RSpec.describe TreeAchievements do
  let(:instance) { described_class.new }
  let(:unprocessed_issues) { JSON.parse(File.read('spec/fixtures/unprocessed_issues.json')) }
  let(:dry_run) { nil }

  before do
    stub_env('DRY_RUN', dry_run)
  end

  describe '#execute' do
    let(:award_response) { {} }

    before do
      allow(instance).to receive(:unprocessed_issues).and_return(unprocessed_issues)
      allow(instance).to receive(:award_achievement).and_return(award_response)
      allow(instance).to receive(:add_label_to_issue)

      instance.execute
    end

    it 'calls unprocessed_issues' do
      expect(instance).to have_received(:unprocessed_issues)
    end

    it 'calls award_achievement the expected number of times with the expected args' do
      expect(instance).to have_received(:award_achievement).with('gid://gitlab/User/222', described_class::TREE_ACHIEVEMENT)
      expect(instance).to have_received(:award_achievement).with('gid://gitlab/User/456', described_class::TREE_ACHIEVEMENT)
      expect(instance).to have_received(:award_achievement).with('gid://gitlab/User/987', described_class::TREE_ACHIEVEMENT)
    end

    it 'calls add_label_to_issue the expected number of times with the expected args' do
      expect(instance).to have_received(:add_label_to_issue).with('gid://gitlab/Issue/111')
      expect(instance).to have_received(:add_label_to_issue).with('gid://gitlab/Issue/123')
      expect(instance).to have_received(:add_label_to_issue).with('gid://gitlab/Issue/999')
    end

    describe 'when award_achievement returns errors' do
      let(:award_response) { { 'errors' => ['error'] } }

      it 'does not call add_label_to_issue' do
        expect(instance).not_to have_received(:add_label_to_issue)
      end
    end
  end

  describe '#unprocessed_issues' do
    before do
      allow(instance).to receive(:execute_graphql).and_return(unprocessed_issues)
    end

    it 'sends the correct query and returns the results' do
      expect(instance.unprocessed_issues).to eq(unprocessed_issues)
    end

    context 'when the query returns an error' do
      let(:unprocessed_issues) { JSON.parse(File.read('spec/fixtures/unprocessed_issues_error.json')) }

      it 'raises an error' do
        expect { instance.unprocessed_issues }.to raise_error(StandardError)
      end
    end
  end

  describe '#add_label_to_issue' do
    before do
      allow(instance).to receive(:execute_graphql)
    end

    it 'does not execute_graphql' do
      instance.add_label_to_issue('gid://gitlab/Issue/111')

      expect(instance).not_to have_received(:execute_graphql)
    end

    context 'when DRY_RUN=0' do
      let(:dry_run) { '0' }

      it 'does execute_graphql' do
        issue_gid = 'gid://gitlab/Issue/111'

        instance.add_label_to_issue(issue_gid)

        # check execute_graphql was called with a payload containing several strings
        expect(instance).to have_received(:execute_graphql)
          .with(/createNote/)
          .with(/noteableId: "#{issue_gid}"/)
      end
    end
  end

  describe '#award_achievement' do
    before do
      allow(instance).to receive(:execute_graphql)
    end

    it 'does not execute_graphql' do
      user_gid = 'gid://gitlab/User/222'

      instance.award_achievement(user_gid, described_class::TREE_ACHIEVEMENT)

      expect(instance).not_to have_received(:execute_graphql)
        .with(/achievementsAward/)
        .with(/achievementId: "#{described_class::TREE_ACHIEVEMENT}"/)
        .with(/userId: "#{user_gid}"/)
    end

    context 'when DRY_RUN=0' do
      let(:dry_run) { '0' }

      it 'does execute_graphql' do
        instance.award_achievement('gid://gitlab/User/222', described_class::TREE_ACHIEVEMENT)

        expect(instance).to have_received(:execute_graphql)
      end
    end
  end
end
