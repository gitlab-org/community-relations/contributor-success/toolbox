# frozen_string_literal: true

require_relative '../bin/contributing_organization_issues'

RSpec.describe ContributingOrganizationIssues do
  let(:credential_path) { '/credential/path' }
  let(:gitlab_client_helper) { instance_double(GitlabClientHelper) }

  before do
    stub_env('GITLAB_API_TOKEN', 'gitlab-access-token')
    stub_env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS', credential_path)
    stub_env('LEADING_ORGS_TRACKER_SHEET_ID', 'SHEET_ID')
    stub_env('DRY_RUN', dry_run)

    # NOTE: This private key is generated and not real!
    file_content = <<-JSON
      {
        "type": "service_account",
        "project_id": "abc123",
        "private_key_id": "1111111111111111111111111111111111111111",
        "private_key": "-----BEGIN PRIVATE KEY-----\\nMIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAv4KNL8dZPL6h0tTP\\nL5WXti24v5osoVv4emCux6S2H4bu3ata8ppqHfcSsJSxiqlBq1sqqx3uVxkZDBuF\\nsJNE9QIDAQABAkBKRE+KUs15cBgDUcHTGzkNTifSLfDW1nrCwpGlHGwARz+3OwKl\\nSFPOVxAhzHdtaHjd/oGL50/qlp3PDJGNPAO1AiEA94o1SVjQ0NBLnJq4Lsm1yU0U\\nvHps+BN+O2jXfyXCXy8CIQDGDh90PZoBBrp5nL120ZeiPwkfObfIds9FP/bfOum1\\nGwIhAJhATJgJZZ4Zj3gJ/aDhdcsTet6WWjGXI7v8txbALbYHAiAaS3O3nhodOsR1\\nMu8goFEOdGoEoEgbMFLycbyYBJ1UswIgX7lVsqWb7WsCslr2kluFzP5aC9wMUvWG\\nUTSIXUN6mqk=\\n-----END PRIVATE KEY-----\\n",
        "client_email": "gitlab@example.com",
        "client_id": "111111111111111111111",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gitlab%40example.iam.gserviceaccount.com",
        "universe_domain": "googleapis.com"
      }
    JSON
    file = instance_double(File, read: file_content)
    allow(File).to receive(:open).with(credential_path).and_return(file)

    stub_request(:post, 'https://www.googleapis.com/oauth2/v4/token').to_return(
      {
        status: 200,
        headers: { 'Content-Type': 'application/json' },
        body: '{"access_token":"token","expires_in":3599,"token_type":"Bearer"}'
      }
    )
    allow(GitlabClientHelper).to receive(:new).and_return(gitlab_client_helper)
  end

  describe '#execute' do
    let(:org_issues_links) do
      ['https://example/web_url/-/issues/3']
    end

    let(:instance) { described_class.new }

    let(:dry_run) { '0' }

    let(:issue) do
      Gitlab::ObjectifiedHash.new(
        {
          description: 'Current description',
          title: 'Issue',
          web_url: 'https://example/web_url/-/issues/3'
        }
      )
    end

    before do
      allow(instance).to receive(:organizations)
      allow(instance).to receive(:populate_issue_links)
      allow(instance).to receive_messages(
        organizations:
          [
            {
              'Issue link' => nil,
              'SFDC_ACCOUNT_ID' => '1',
              'CONTRIBUTOR_ORGANIZATION' => 'ABC Co. 1'
            }
          ]
      )
    end

    context 'when issue is not present in the sheet row' do
      before do
        allow(instance).to receive_messages(
          organizations:
            [
              {
                'Issue link' => nil,
                'SFDC_ACCOUNT_ID' => '1',
                'CONTRIBUTOR_ORGANIZATION' => 'ABC Co. 1'
              }
            ]
        )
      end

      it 'calls the expected methods with the expected values when issue already exists' do
        allow(gitlab_client_helper).to receive(:search_project_issues).and_return([issue])

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(org_issues_links)
      end

      it 'calls the expected methods with the expected values when issue does not exist' do
        allow(instance).to receive_messages(find_issue: nil)
        allow(gitlab_client_helper).to receive_messages(create_issue: issue)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(org_issues_links)
      end
    end

    context 'when issue is present in the sheet row' do
      before do
        allow(instance).to receive_messages(
          organizations:
            [
              {
                'Issue link' => 'https://example/web_url/-/issues/3',
                'SFDC_ACCOUNT_ID' => '1',
                'CONTRIBUTOR_ORGANIZATION' => 'ABC Co. 1'
              }
            ]
        )
      end

      it 'calls the expected methods with the expected values' do
        allow(gitlab_client_helper).to receive(:get_issue).with(62_760_330, '3').and_return(issue)
        allow(gitlab_client_helper).to receive_messages(edit_issue: issue)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(org_issues_links)
      end

      it 'returns a failure message when issue was not found' do
        allow(gitlab_client_helper).to receive(:get_issue).and_raise(Gitlab::Error::NotFound)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(['Failed to load issue for account 1'])
      end

      it 'returns a failure message when StandardError occurred while getting the issue' do
        allow(gitlab_client_helper).to receive(:get_issue).and_raise(StandardError)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(['Failed to load issue for account 1'])
      end

      context 'when the issue does not need changing' do # rubocop:disable RSpec/NestedGroups
        let(:issue) do
          Gitlab::ObjectifiedHash.new(
            {
              description: 'Current description',
              title: 'ABC Co. 1 - 1',
              web_url: 'https://example/web_url/-/issues/3'
            }
          )
        end

        it 'does not update the issue' do
          allow(gitlab_client_helper).to receive(:get_issue).with(62_760_330, '3').and_return(issue)

          instance.execute

          expect(instance).to have_received(:populate_issue_links).with(org_issues_links)
        end
      end
    end

    context 'when DRY_RUN = 1 and issue is not present in the sheet row' do
      let(:issue) do
        Gitlab::ObjectifiedHash.new(
          {
            description: 'Current description',
            title: 'Issue',
            web_url: 'https://example/web_url/-/issues/3'
          }
        )
      end
      let(:dry_run) { '1' }

      before do
        allow(instance).to receive_messages(
          organizations:
            [
              {
                'Issue link' => nil,
                'SFDC_ACCOUNT_ID' => '1',
                'CONTRIBUTOR_ORGANIZATION' => 'ABC Co. 1'
              }
            ]
        )
      end

      it 'calls the expected methods with the expected values when issue already exists' do
        allow(instance).to receive_messages(find_issue: issue)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(['Found an existing issue for the account 1'])
      end

      it 'calls the expected methods with the expected values when issue does not exist' do
        allow(instance).to receive_messages(find_issue: nil)
        allow(gitlab_client_helper).to receive_messages(create_issue: issue)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(['Creating a new issue for the account 1'])
      end
    end

    context 'when DRY_RUN = 1 and issue is present in the sheet row' do
      let(:dry_run) { '1' }

      before do
        allow(instance).to receive_messages(
          organizations:
            [
              {
                'Issue link' => 'https://example/web_url/-/issues/3',
                'SFDC_ACCOUNT_ID' => '1',
                'CONTRIBUTOR_ORGANIZATION' => 'ABC Co. 1'
              }
            ]
        )
      end

      it 'calls the expected methods with the expected values' do
        allow(gitlab_client_helper).to receive(:get_issue).with(62_760_330, '3').and_return(issue)

        instance.execute

        expect(instance).to have_received(:populate_issue_links).with(['Updating issue https://example/web_url/-/issues/3 with new contributor info: ABC Co. 1 - 1'])
      end
    end
  end
end
