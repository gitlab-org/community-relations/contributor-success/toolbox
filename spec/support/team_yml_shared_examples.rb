# frozen_string_literal: true

require_relative '../../lib/team_yml_helper'

RSpec.shared_context 'with team yml processing context' do
  let(:team_yml) do
    <<~TEAMYML
      ---
      - slug: slug1
        type: person
        name: name1
        role: Chief Tester
        reports_to: slug1
        other_things: ignored attribute
        pronouns:
        pronunciation:
        twitter:
        linkedin: SuperProfessionalProfile
        gitlab: user1
        projects:
          project-name:
            - maintainer
      - slug: slug2
        type: person
        name: name2
        role: Under Tester
        other_things: ignored attribute
        pronouns:
        pronunciation:
        twitter: notARealTwit
        linkedin:
        reports_to: slug1
        gitlab: user2
        projects:
      - slug: tester3
        type: person
        name: Anonymous Tester
        role: <a href="https://handbook.example.com/job-families/anonymous-tester/">Anonymous
          Tester</a> <br />Other Team member
        other_things: ignored attribute
        pronouns: they/them
        pronunciation: uh no nuh muhs teh stuh
        twitter:
        linkedin:
        reports_to: a-mgr
        gitlab: AnonymousTester
        projects:
      - slug: a-mgr
        type: person
        name: Anonymous Manager
        role: <a href="https://handbook.example.com/job-families/anonymous-manager/">Anonymous
          Manager</a>
        other_things: ignored attribute
        pronouns:
        pronunciation: uh no nuh muhs man a juh
        twitter: i-tweet-about-mgmt
        linkedin:
        reports_to: slug1
        gitlab: also-anonymous
        projects:
    TEAMYML
  end

  let(:parsed_team) do
    {
      'user1' => {
        gitlab: 'user1',
        name: 'name1',
        reports_to: 'slug1',
        role: 'Chief Tester',
        slug: 'slug1',
        username: 'user1',
        projects: { 'project-name' => ['maintainer'] }
      },
      'user2' => {
        gitlab: 'user2',
        name: 'name2',
        reports_to: 'slug1',
        role: 'Under Tester',
        slug: 'slug2',
        username: 'user2',
        projects: nil
      },
      'anonymoustester' => {
        gitlab: 'AnonymousTester',
        name: 'Anonymous Tester',
        reports_to: 'a-mgr',
        role: '<a href="https://handbook.example.com/job-families/anonymous-tester/">Anonymous Tester</a> <br />Other Team member',
        slug: 'tester3',
        username: 'AnonymousTester',
        projects: nil
      },
      'also-anonymous' => {
        gitlab: 'also-anonymous',
        name: 'Anonymous Manager',
        reports_to: 'slug1',
        role: '<a href="https://handbook.example.com/job-families/anonymous-manager/">Anonymous Manager</a>',
        slug: 'a-mgr',
        username: 'also-anonymous',
        projects: nil
      }
    }
  end

  before do
    allow(HTTParty).to receive(:get).with(TeamYml::WWW_GITLAB_COM_TEAM_YML).and_return(team_yml)
  end
end
