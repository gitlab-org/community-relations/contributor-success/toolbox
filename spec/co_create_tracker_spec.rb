# frozen_string_literal: true

require_relative '../bin/co_create_tracker'

RSpec.describe CoCreateTracker do
  let(:credential_path) { '/credential/path' }
  let(:gitlab_client_helper) { instance_double(GitlabClientHelper) }
  let(:google_sheets_helper) { instance_double(GoogleSheetsHelper) }

  before do
    stub_env('GOOGLE_SERVICE_ACCOUNT_CREDENTIALS', credential_path)
    stub_env('COCREATE_GOOGLE_SERVICE_ACCOUNT_CREDENTIALS', credential_path)

    stub_env('LEADING_ORGS_TRACKER_SHEET_ID', 'SHEET_ID')
    stub_env('CO_CREATE_ORGS_TRACKER_SHEET_ID', 'SHEET_ID')
    stub_env('CO_CREATE_REPORT_SHEET_ID', 'SHEET_ID')

    stub_env('PERIOD_START_DATE', '2024-11-01T00:00:00.938Z')
    stub_env('PERIOD_END_DATE', '2025-02-01T00:00:00.446Z')

    stub_env('DRY_RUN', dry_run)
  end

  describe '#execute' do
    let(:instance) { described_class.new }
    let(:dry_run) { '0' }
    let(:opened_merge_requests) do
      Gitlab::PaginatedResponse.new(
        [
          Gitlab::ObjectifiedHash.new(
            {
              description: 'Opened during the period',
              project_id: 23,
              iid: 2,
              web_url: 'https://example/opened_web_url_1',
              created_at: '2024-12-01T00:00:00.938Z',
              merged_at: nil,
              labels: 'type::feature',
              state: 'opened',
              author_username: 'username_1'
            }
          ),
          Gitlab::ObjectifiedHash.new(
            {
              description: 'Opened before',
              project_id: 23,
              iid: 2,
              web_url: 'https://example/opened_web_url_2',
              created_at: '2024-10-01T00:00:00.938Z',
              merged_at: nil,
              labels: 'type::feature',
              state: 'opened',
              author_username: 'username_1'
            }
          )
        ]
      )
    end
    let(:merged_merge_requests) do
      Gitlab::PaginatedResponse.new(
        [
          Gitlab::ObjectifiedHash.new(
            {
              description: 'Merged during the period',
              project_id: 23,
              iid: 2,
              web_url: 'https://example/merged_web_url_1',
              merged_at: '2024-12-01T00:00:00.938Z',
              labels: 'type::feature',
              state: 'merged',
              author_username: 'username_1'
            }
          )
        ]
      )
    end
    let(:opened_mrs_params) do
      {
        author_username: 'username_1',
        state: 'opened',
        labels: 'type::feature'
      }
    end
    let(:merged_mrs_params) do
      {
        author_username: 'username_1',
        state: 'merged',
        labels: 'type::feature'
      }
    end
    let(:get_spreadsheet_response) do
      Google::Apis::SheetsV4::Spreadsheet.new(
        sheets: [
          Google::Apis::SheetsV4::Sheet.new(properties: Google::Apis::SheetsV4::SheetProperties.new(title: '2024-11-01 - 2025-02-01'))
        ]
      )
    end

    before do
      allow(GoogleSheetsHelper).to receive(:new).and_return(google_sheets_helper)
      allow(GitlabClientHelper).to receive(:new).and_return(gitlab_client_helper)
      allow(google_sheets_helper).to receive(:fetch_spreadsheet).and_return(get_spreadsheet_response)
      allow(google_sheets_helper).to receive(:clear_values)

      allow(instance).to receive_messages(
        read_co_create_sheet: [
          {
            'Company' => 'Company 1',
            'Stage' => 'Stage',
            'Had onsite' => 'No',
            'DRI (CSM or SA)' => 'John',
            'CRM account id' => '12345'
          },
          {
            'Company' => 'Company 2',
            'Stage' => 'Not Qualified',
            'Had onsite' => 'No',
            'DRI (CSM or SA)' => 'Robin',
            'CRM account id' => '12347'
          }
        ]
      )
      allow(instance).to receive_messages(
        fetch_mrarr_organizations: [
          {
            'CONTRIBUTOR_ORGANIZATION' => 'Org 1',
            'SFDC_ACCOUNT_ID' => '12345',
            'CONTRIBUTOR_USERNAMES' => "[ \n  \"username_1\"\n ]"
          },
          {
            'CONTRIBUTOR_ORGANIZATION' => 'Org 2',
            'SFDC_ACCOUNT_ID' => '12347',
            'CONTRIBUTOR_USERNAMES' => "[ \n  \"username_3\", \n  \"username_4\"\n ]"
          }
        ]
      )

      allow(gitlab_client_helper).to receive(:group_merge_requests).with(CoCreateTracker::GITLAB_ORG_GROUP_ID, merged_mrs_params).and_return(merged_merge_requests)
      allow(gitlab_client_helper).to receive(:group_merge_requests).with(CoCreateTracker::GITLAB_ORG_GROUP_ID, opened_mrs_params).and_return(opened_merge_requests)
    end

    context 'when the tab with a name already exists in the speadsheet' do
      it 'clears previous data and populates new ones' do
        expect(google_sheets_helper).to receive(:write_values).with(
          1,
          'A',
          [CoCreateTracker::COCREATE_REPORT_HEADER, ['Company 1', '12345', 'No', 'John', 'https://example/merged_web_url_1', 'https://example/opened_web_url_1']]
        )
        expect { instance.execute }.not_to raise_error
        expect(google_sheets_helper).to have_received(:clear_values)
      end
    end

    context 'when DRY_RUN = 1' do
      let(:dry_run) { '1' }

      it 'does not error, does not send clear and populate requests' do
        expect(google_sheets_helper).not_to receive(:write_values).with(
          1,
          'A',
          [CoCreateTracker::COCREATE_REPORT_HEADER, ['Company 1', '12345', 'No', 'John', 'https://example/merged_web_url_1', 'https://example/opened_web_url_1']]
        )
        expect { instance.execute }.not_to raise_error
        expect(google_sheets_helper).not_to have_received(:clear_values)
      end
    end

    context 'when there are no merge requests' do
      let(:merged_merge_requests) { Gitlab::PaginatedResponse.new([]) }
      let(:opened_merge_requests) { Gitlab::PaginatedResponse.new([]) }

      it 'returns a message indicating nothing to report' do
        result = instance.execute
        expect(result).to be_nil
      end
    end

    context 'when fetch organization merge requests fails with StandardError' do
      before do
        # changing to 1 for test to run faster
        stub_const('CoCreateTracker::MAX_RETRIES', 1)
        stub_const('CoCreateTracker::RETRY_DELAY', 1)
        allow(gitlab_client_helper).to receive(:group_merge_requests).and_raise(StandardError)
      end

      it 'returns a message indicating nothing to report' do
        result = instance.execute
        expect(result).to be_nil
      end
    end

    context 'when merge requests merged outside of the time period' do
      let(:opened_merge_requests) { Gitlab::PaginatedResponse.new([]) }
      let(:merged_merge_requests) do
        Gitlab::PaginatedResponse.new(
          [
            Gitlab::ObjectifiedHash.new(
              {
                description: 'Current description',
                project_id: 23,
                iid: 2,
                web_url: 'https://example/web_url',
                merged_at: '2022-12-01T00:00:00.938Z', # merged before the specified time
                labels: 'type::feature',
                state: 'merged',
                author_username: 'username_1'
              }
            )
          ]
        )
      end

      it 'returns a message - nothing to report' do
        result = instance.execute
        expect { instance.execute }.not_to raise_error
        expect(result).to be_nil
      end
    end

    context 'when the tab with a name doeas not exist in the speadsheet' do
      let(:get_spreadsheet_response) do
        Google::Apis::SheetsV4::Spreadsheet.new(
          sheets: [
            Google::Apis::SheetsV4::Sheet.new(properties: Google::Apis::SheetsV4::SheetProperties.new(title: 'some title'))
          ]
        )
      end

      it 'populates data to a new tab' do
        expect(google_sheets_helper).to receive(:add_new_tab).with('2024-11-01 - 2025-02-01')
        expect(google_sheets_helper).to receive(:write_values).with(
          1,
          'A',
          [CoCreateTracker::COCREATE_REPORT_HEADER, ['Company 1', '12345', 'No', 'John', 'https://example/merged_web_url_1', 'https://example/opened_web_url_1']]
        )

        expect { instance.execute }.not_to raise_error
        expect(google_sheets_helper).not_to have_received(:clear_values)
      end
    end
  end
end
