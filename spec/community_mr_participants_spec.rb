# frozen_string_literal: true

require_relative '../bin/community_mr_participants'

RSpec.describe CommunityMrParticipants do
  include_context 'with team yml processing context'

  let(:errors) { [] }
  let(:org_data) do
    {
      'group' => {
        'mergeRequests' => {
          'edges' => [
            {
              'node' => {
                'author' => { 'username' => 'user1', 'name' => 'Alex', 'discord' => '123456789' },
                'reviewers' => { 'nodes' => [] },
                'approvedBy' => { 'nodes' => [] },
                'commenters' => {
                  'nodes' => [
                    { 'username' => 'user4', 'name' => 'Robin', 'discord' => '' },
                    { 'username' => 'AnonymousTester', 'name' => 'Anonymous Tester' }
                  ]
                }
              }
            },
            {
              'node' => {
                'author' => { 'username' => 'user2', 'name' => 'Purna' },
                'reviewers' => { 'nodes' => [] },
                'approvedBy' => { 'nodes' => [] },
                'commenters' => {
                  'nodes' => [
                    { 'username' => 'user4', 'name' => 'Robin', 'discord' => '' },
                    { 'username' => 'AnonymousTester', 'name' => 'Anonymous Tester' }
                  ]
                }
              }
            }
          ],
          'pageInfo' => {}
        }
      }
    }
  end
  let(:com_data) do
    {
      'group' => {
        'mergeRequests' => {
          'edges' => [
            {
              'node' => {
                'author' => { 'username' => 'user2', 'name' => 'Purna' },
                'reviewers' => { 'nodes' => [] },
                'approvedBy' => { 'nodes' => [] },
                'commenters' => { 'nodes' => [{ 'username' => 'AnonymousTester', 'name' => 'Anonymous Tester', 'discord' => '' }] }
              }
            },
            {
              'node' => {
                'author' => { 'username' => 'user3', 'name' => 'Jordan', 'discord' => '987654321' },
                'reviewers' => { 'nodes' => [] },
                'approvedBy' => { 'nodes' => [] },
                'commenters' => { 'nodes' => [{ 'username' => 'AnonymousTester', 'name' => 'Anonymous Tester', 'discord' => '' }] }
              }
            }
          ],
          'pageInfo' => {}
        }
      }
    }
  end
  let(:args) do
    args = Options.new
    args.scheduled = true
    args.start_date = Date.new(2023, 6, 22)
    args.end_date = Date.new(2023, 6, 29)
    args.page_size = 25
    args
  end

  let(:gitlab_client_helper) { instance_double(GitlabClientHelper) }
  let(:project_id) { 39_971_471 }
  let(:issue_iid) { 85 }
  let(:issue) do
    Gitlab::ObjectifiedHash.new(
      {
        description: 'Current description',
        project_id: project_id,
        iid: issue_iid,
        web_url: 'https://example/web_url'
      }
    )
  end
  let(:description_addition) { '- [ ] [2023-06-29](https://gitlab.com/job/url) - (3 GitLab team members helped 4 MRs)' }

  shared_examples 'behaves as expected' do
    it 'behaves as expected' do
      expect { described_class.new(args).start(args) }.not_to raise_error
    end
  end

  before do
    stub_env('CI_API_GRAPHQL_URL', 'https://example.com/graphql')
    stub_env('GITLAB_API_TOKEN', 'token')
    stub_env('SLACK_CONTRIBUTOR_SUCCESS_WEBHOOK_URL', 'https://slack.com/webhook')
    stub_env('CI_JOB_URL', 'https://gitlab.com/job/url')

    org_response = instance_double(GraphQL::Client::Response)
    allow(org_response).to receive(:errors).and_return(errors)
    allow(org_response).to receive(:data).and_return(org_data)
    allow(org_response).to receive(:extensions).and_return({})

    com_response = instance_double(GraphQL::Client::Response)
    allow(com_response).to receive(:errors).and_return(errors)
    allow(com_response).to receive(:data).and_return(com_data)
    allow(com_response).to receive(:extensions).and_return({})

    client = instance_double(GraphQL::Client)
    allow(client).to receive(:allow_dynamic_queries=)
    allow(client).to receive(:parse)
    allow(client).to receive(:query).with(nil, a_hash_including(variables: a_hash_including(group_path: 'gitlab-org'))).and_return(org_response)
    allow(client).to receive(:query).with(nil, a_hash_including(variables: a_hash_including(group_path: 'gitlab-com'))).and_return(com_response)

    allow(GraphQL::Client).to receive(:load_schema).and_return(true)
    allow(GraphQL::Client).to receive(:new).and_return(client)

    allow(GitlabClientHelper).to receive(:new).and_return(gitlab_client_helper)
    allow(gitlab_client_helper).to receive(:append_issue_description).with(issue, description_addition)
    allow(gitlab_client_helper).to receive(:get_group_members).with(62_281_442).and_return([])
    allow(gitlab_client_helper).to receive(:get_issue).with(project_id, issue_iid).and_return(issue)
    allow(gitlab_client_helper).to receive(:user_blocked_or_not_exists?).with('user1').and_return(false)
    allow(gitlab_client_helper).to receive(:user_blocked_or_not_exists?).with('user2').and_return(false)
    allow(gitlab_client_helper).to receive(:user_blocked_or_not_exists?).with('user3').and_return(false)
    allow(gitlab_client_helper).to receive(:user_blocked_or_not_exists?).with('user4').and_return(false)
    allow(gitlab_client_helper).to receive(:user_blocked_or_not_exists?).with('AnonymousTester').and_return(false)
    allow(gitlab_client_helper).to receive(:name_from_username).with('AnonymousTester').and_return('Real name')
    allow(gitlab_client_helper).to receive(:name_from_username).with('also-anonymous').and_return('Actual Manager')

    stub_request(:post, 'https://slack.com/webhook')
      .with do |request|
        request.body.include?(output.gsub("\n", '\\n'))
      end.to_return(status: 200, body: '', headers: {})
  end

  context 'when scheduled team member report' do
    let(:output) do
      <<~EOTEXT.chomp
        https://example/web_url
        ```
        Community MR Participants
        From:       2023-06-22
        To:         2023-06-29
        CSV output: false
        Page Size:  25
        Paging:     true
        Debug mode: false
        Scope:      GitLab Team mode

        ** Paste this message into slack using Cmd-v, then apply formatting using Cmd-Shift-F **

        :heart: :tada: Weekly Wider Contribution Appreciation :tada: :heart:
        We are striving toward 170 unique monthly wider community contributors by 2025-01-01 and appreciate every effort supporting our wider community.
        If you have any ideas/feedback/concerns please head over to #contributor-success.

        Thank you to all 3 GitLab team members who helped get one of the 4 MRs (contributed by 1 authors) merged from 2023-06-22 to 2023-06-29.

        Top performers (3+ interactions! We're also mentioning your managers to increase awareness of these awesome results :rocket:)
        @Real name

        High performers (2 interactions!)
        @name2

        Thank you to these wonderful additional team members for helping our community members in the past week (1 interaction)

        name1

        cc @esalvador, @John Coghlan, @Nick Veenhof
        cc (Managers of Top Performers :rocket:) @Actual Manager
      EOTEXT
    end

    it_behaves_like 'behaves as expected'
  end

  context 'when scheduled wider community report' do
    let(:issue_iid) { 186 }
    let(:description_addition) { '| [2023-06-29](https://gitlab.com/job/url) | 1 | 1 | 3 | 2 |' }
    let(:output) { 'Without environment variables set, nothing should be posted to slack!' }

    before do
      args.wider_community = true
    end

    it_behaves_like 'behaves as expected'

    context 'with DISCORD_THANKS_WEBHOOK_PATH environment variable set' do
      before do
        stub_env('DISCORD_THANKS_WEBHOOK_PATH', 'webhooks/1234/SeCrEt')
        discord_message1 = <<~EOTEXT.chomp
          :heart: :tada: Community Contribution Appreciation :tada: :heart:
          We are striving toward 170 unique monthly wider community contributors by 2025-01-01 and appreciate every effort from the community towards this goal.
          If you have any ideas/feedback/concerns please feel free to discuss them here!

          Thank you to all 1 wider community members who **AUTHORED** merge requests that were merged from 2023-06-22 to 2023-06-29.
          There were a total of 1 community contributions!

          Top authors (3+ merge requests :rocket:)


          Regular authors (2 merge requests!)


          Additional authors (1 merge request)
          <@987654321> (`@user3`)

        EOTEXT
        discord_message2 = <<~EOTEXT.chomp
          Additionally, thank you to all 2 wider community members who participated/reviewed _other_ merge requests, merged from 2023-06-22 to 2023-06-29.

          Top performers (3+ interactions :rocket:)


          High performers (2 interactions!)
          Robin (`@user4`)

          Additional contributors (1 interaction)
          <@987654321> (`@user3`)
        EOTEXT

        stub_request(:post, 'https://discord.com/api/webhooks/1234/SeCrEt')
          .with do |request|
            request.body.include?(discord_message1.gsub("\n", '\\n'))
          end.to_return(status: 200, body: '', headers: {})
        stub_request(:post, 'https://discord.com/api/webhooks/1234/SeCrEt')
          .with do |request|
            request.body.include?(discord_message2.gsub("\n", '\\n'))
          end.to_return(status: 200, body: '', headers: {})
      end

      it_behaves_like 'behaves as expected'
    end

    context 'with DISCOURSE_API_KEY set' do
      before do
        stub_env('DISCOURSE_API_KEY', 'ABC123')

        forum_message = <<~EOTEXT.chomp
          :heart: :tada: Community Contribution Appreciation :tada: :heart:
          We are striving toward 170 unique monthly wider community contributors by 2025-01-01 and appreciate every effort from the community towards this goal.
          If you have any ideas/feedback/concerns please feel free to discuss them here!

          Thank you to all 1 wider community members who **AUTHORED** merge requests that were merged from 2023-06-22 to 2023-06-29.
          There were a total of 1 community contributions!

          Top authors (3+ merge requests :rocket:)


          Regular authors (2 merge requests!)


          Additional authors (1 merge request)
          [Jordan](https://gitlab.com/user3)

          ---
          Additionally, thank you to all 2 wider community members who participated/reviewed _other_ merge requests, merged from 2023-06-22 to 2023-06-29.

          Top performers (3+ interactions :rocket:)


          High performers (2 interactions!)
          [Robin](https://gitlab.com/user4)

          Additional contributors (1 interaction)
          [Jordan](https://gitlab.com/user3)
        EOTEXT

        stub_request(:post, 'https://forum.gitlab.com/posts.json')
          .with do |request|
          request.body.include?(forum_message.gsub("\n", '\\n'))
        end.to_return(status: 200, body: '', headers: {})
      end

      it_behaves_like 'behaves as expected'
    end
  end
end
