# frozen_string_literal: true

require_relative '../bin/mr_linked_issue_labeller'

# We return 3 merge requests (across 2 pages)
# This ensures pagination is correctly handled
# MR IID | Labelled? | Linked? | Result
# 1694   | No        | Yes     | Add label
# 132478 | No        | No      | No action
# 132232 | Yes       | Yes     | No action
# 132460 | Yes       | No      | Remove label
RSpec.describe MrLinkedIssueLabeller do
  def graphql_request(after = '')
    <<~EOGQL.chomp.gsub("\n", '\\n').gsub('"', '\\\"')
      query {
        group(fullPath: "gitlab-org") {
          mergeRequests(
            labels: ["Community contribution"]
            updatedAfter: "2023-09-25T14:17:00Z"
            includeSubgroups: true
            after: "#{after}"
            ) {
            count
            pageInfo {
              hasNextPage
              endCursor
            }
            nodes {
              projectId
              iid
              webUrl
              labels {
                nodes {
                  title
                }
              }
            }
          }
        }
    EOGQL
  end

  before do
    stub_env('CI_API_GRAPHQL_URL', 'https://gitlab/gql')
    stub_env('GITLAB_API_TOKEN', 'token')
    stub_env('PRODUCTION', 'true')

    graphql_response_page1 = <<~EOGQL.chomp
      {
        "data": {
          "group": {
            "mergeRequests": {
              "count": 6,
              "pageInfo": {
                "hasNextPage": true,
                "endCursor": "eyJjcmVhdGVkX2F0IjoiMjAyMy0wOS0yMCAxMTowMTozMy40ODI1NTAwMDAgKzAwMDAiLCJpZCI6IjI1MjAwNTg0NSJ9"
              },
              "nodes": [
                {
                  "projectId": 40916776,
                  "iid": "1694",
                  "webUrl": "https://gitlab.com/gitlab-org/terraform-provider-gitlab/-/merge_requests/1694",
                  "labels": {
                    "nodes": [
                      {
                        "title": "Community contribution"
                      },
                      {
                        "title": "bug::functional"
                      }
                    ]
                  }
                },
                {
                  "projectId": 278964,
                  "iid": "132478",
                  "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132478",
                  "labels": {
                    "nodes": [
                      {
                        "title": "Community contribution"
                      }
                    ]
                  }
                },
                {
                  "projectId": 278964,
                  "iid": "132232",
                  "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132232",
                  "labels": {
                    "nodes": [
                      {
                        "title": "Community contribution"
                      },
                      {
                        "title": "linked-issue"
                      }
                    ]
                  }
                }
              ]
            }
          }
        }
      }
    EOGQL
    graphql_response_page2 = <<~EOGQL.chomp
      {
        "data": {
          "group": {
            "mergeRequests": {
              "count": 6,
              "pageInfo": {
                "hasNextPage": false,
                "endCursor": "end"
              },
              "nodes": [
                {
                  "projectId": 278964,
                  "iid": "132460",
                  "webUrl": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132460",
                  "labels": {
                    "nodes": [
                      {
                        "title": "Community contribution"
                      },
                      {
                        "title": "linked-issue"
                      }
                    ]
                  }
                }
              ]
            }
          }
        }
      }
    EOGQL

    stub_request(:post, 'https://gitlab/gql')
      .with(
        headers: {
          'Authorization' => 'Bearer token',
          'Content-Type' => 'application/json',
          'User-Agent' => 'Contributor Success Toolbox'
        }
      ) do |request|
      request.body.include?(graphql_request)
    end.to_return(body: graphql_response_page1)
    stub_request(:post, 'https://gitlab/gql')
      .with(
        headers: {
          'Authorization' => 'Bearer token',
          'Content-Type' => 'application/json',
          'User-Agent' => 'Contributor Success Toolbox'
        }
      ) do |request|
      request.body.include?(graphql_request('eyJjcmVhdGVkX2F0IjoiMjAyMy0wOS0yMCAxMTowMTozMy40ODI1NTAwMDAgKzAwMDAiLCJpZCI6IjI1MjAwNTg0NSJ9'))
    end.to_return(body: graphql_response_page2)
    stub_request(:get, 'https://gitlab.com/gitlab-org/terraform-provider-gitlab/-/merge_requests/1694')
      .to_return(body: "a\nwindow.gl.mrWidgetData = { \"issues_links\": { \"closing_count\": 1 } }\nb")
    stub_request(:get, 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132478')
      .to_return(body: "a\nwindow.gl.mrWidgetData = {}\nb")
    stub_request(:get, 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132232')
      .to_return(body: "a\nwindow.gl.mrWidgetData = { \"issues_links\": { \"mentioned_count\": 3 } }\nb")
    stub_request(:get, 'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/132460')
      .to_return(body: "a\nwindow.gl.mrWidgetData = {}\nb")
    stub_request(:post, 'https://gitlab.com/api/v4/projects/278964/merge_requests/132460/notes')
      .with(
        body: '{"body":"/unlabel ~linked-issue"}',
        headers: { 'Authorization' => 'Bearer token' }
      )
    stub_request(:post, 'https://gitlab.com/api/v4/projects/40916776/merge_requests/1694/notes')
      .with(
        body: '{"body":"/label ~linked-issue"}',
        headers: { 'Authorization' => 'Bearer token' }
      )
  end

  it 'makes the expected REST and GraphQL API calls' do
    updated_after = Time.utc(2023, 9, 25, 14, 17, 0)

    expect { described_class.new('gitlab-org', updated_after).execute }.not_to raise_error
  end
end
