provider "gitlab" {
  # Authentication received from GITLAB_TOKEN env var
}

module "team_member_achievements" {
  source = "./module/schedule"

  description = "[Every Day @5am] - TEAM_MEMBER_ACHIEVEMENTS"
  cron        = "0 5 * * *"
  timezone    = "Europe/London"

  pipeline_variables = {
    "PIPELINE_NAME"            = "Team member Achievements Scheduled run",
    "TEAM_MEMBER_ACHIEVEMENTS" = "1",
    "GITLAB_API_TOKEN"         = var.team_member_achievements_token
  }
}

module "maintainer_achievements" {
  source = "./module/schedule"

  description = "[Every Day @3pm] - MAINTAINER_ACHIEVEMENTS"
  cron        = "0 15 * * *"
  timezone    = "Europe/London"

  pipeline_variables = {
    "PIPELINE_NAME"           = "Maintainer Achievements Scheduled run",
    "MAINTAINER_ACHIEVEMENTS" = "1",
    "GITLAB_API_TOKEN"        = var.team_member_achievements_token
  }
}

module "merger_achievements" {
  source = "./module/schedule"

  description = "[Every Day @3pm] - MERGER_ACHIEVEMENTS"
  cron        = "0 15 * * *"
  timezone    = "Europe/London"

  pipeline_variables = {
    "PIPELINE_NAME"       = "Merger Achievements Scheduled run",
    "MERGER_ACHIEVEMENTS" = "1",
    "GITLAB_API_TOKEN"    = var.team_member_achievements_token
  }
}

module "one_hundred_reviews_achievements" {
  source = "./module/schedule"

  description = "[Every Day @3pm] - ONE_HUNDRED_REVIEWS_ACHIEVEMENTS"
  cron        = "0 15 * * *"
  timezone    = "Europe/London"

  pipeline_variables = {
    "PIPELINE_NAME"                    = "100 Reviews Achievements Scheduled run",
    "ONE_HUNDRED_REVIEWS_ACHIEVEMENTS" = "1",
    "GITLAB_API_TOKEN"                 = var.team_member_achievements_token
  }
}

module "fast_review_achievements" {
  source = "./module/schedule"

  description = "[Every Day @3pm] - FAST_REVIEW_ACHIEVEMENTS"
  cron        = "0 15 * * *"
  timezone    = "Europe/London"

  pipeline_variables = {
    "PIPELINE_NAME"            = "Fast Review Achievements Scheduled run",
    "FAST_REVIEW_ACHIEVEMENTS" = "1",
    "GITLAB_API_TOKEN"         = var.gitlab_achievements_token
  }
}

module "wider_mr_participants" {
  source = "./module/schedule"

  description = "[Every Monday @4am] - WIDER_MR_PARTICIPANTS"
  cron        = "0 4 * * 1"

  pipeline_variables = {
    "PIPELINE_NAME"               = "Community MR Participants Scheduled run",
    "WIDER_MR_PARTICIPANTS"       = "1"
    "DISCORD_THANKS_WEBHOOK_PATH" = var.discord_thanks_webhook_path
    "DISCOURSE_API_KEY"           = var.discourse_api_key
    "DRY_RUN"                     = "0"
  }
}

module "community_mr_participants" {
  source = "./module/schedule"

  description = "[Every Monday @3am] - COMMUNITY_MR_PARTICIPANTS"
  cron        = "0 3 * * 1"

  pipeline_variables = {
    "PIPELINE_NAME"             = "Community MR Participants Scheduled run",
    "COMMUNITY_MR_PARTICIPANTS" = "1"
  }
}

module "mr_linked_issue_labeller" {
  source = "./module/schedule"

  description = "[Every 5 Minutes] - MR_LINKED_ISSUE_LABELLER"
  cron        = "*/5 * * * *"

  pipeline_variables = {
    "PIPELINE_NAME"            = "MR Linked Issue Labeller Scheduled run",
    "MR_LINKED_ISSUE_LABELLER" = "true",
    "PRODUCTION"               = "true"
  }
}

module "leading_organization_identifier" {
  source = "./module/schedule"

  description = "[Every Day @4.30am] - LEADING_ORGANIZATION_IDENTIFIER"
  cron        = "30 4 * * *"

  pipeline_variables = {
    "PIPELINE_NAME"                   = "Leading Org Scheduled run",
    "LEADING_ORGANIZATION_IDENTIFIER" = "1"
  }
}

module "contributing_organization_issues" {
  source = "./module/schedule"

  description = "[Every Day @5:30am] - CONTRIBUTING ORGANIZATION ISSUES"
  cron        = "30 5 * * *"
  timezone    = "Europe/London"

  pipeline_variables = {
    "CONTRIBUTING_ORGANIZATION_ISSUES" = "1"
    "DRY_RUN"                          = "0"
    "GITLAB_API_TOKEN"                 = var.contributing_organization_issues_token
  }
}
