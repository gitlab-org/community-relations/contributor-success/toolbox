resource "gitlab_pipeline_schedule" "schedule" {
  project       = 44798062
  description   = var.description
  ref           = "refs/heads/main"
  cron          = var.cron
  cron_timezone = var.timezone

  take_ownership = true
}

resource "gitlab_pipeline_schedule_variable" "variable" {
  for_each = var.pipeline_variables

  project              = gitlab_pipeline_schedule.schedule.project
  pipeline_schedule_id = gitlab_pipeline_schedule.schedule.pipeline_schedule_id
  key                  = each.key
  value                = each.value
}
