variable "description" {
  type        = string
  description = "Description of the pipeline schedule"
}

variable "cron" {
  type        = string
  description = "Standard Crontab syntax for job execution schedule"
}

variable "pipeline_variables" {
  type        = map(string)
  description = "K/V map of the variables"
}

variable "timezone" {
  type        = string
  description = "Timezone value if different to Etc/UTC"
  default     = "Etc/UTC"
}
