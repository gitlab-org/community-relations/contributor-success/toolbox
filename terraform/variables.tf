variable "tree_achievements_token" {
  type        = string
  description = "GitLab PAT for authentication. Requires maintainer role in the achievements group and developer role in the rewards project."
  sensitive   = true
  default     = ""
}

variable "team_member_achievements_token" {
  type        = string
  description = "GitLab PAT for authentication"
  sensitive   = true
}

variable "gitlab_achievements_token" {
  type        = string
  description = "GitLab PAT for authentication"
  sensitive   = true
  default     = ""
}

variable "discord_thanks_webhook_path" {
  type        = string
  description = "Path for Discord #thanks channel webhook"
  sensitive   = true
}

variable "discourse_api_key" {
  type        = string
  description = "Discourse API key"
  sensitive   = true
}

variable "community_pairing_zoom_link" {
  type        = string
  description = "Link to the zoom meeting for the community pairing"
  sensitive   = true
}

variable "contributing_organization_issues_token" {
  type        = string
  description = "GitLab PAT for authentication"
  sensitive   = true
  default     = ""
}
